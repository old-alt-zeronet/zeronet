const astUtils = require("eslint/lib/rules/utils/ast-utils");

module.exports = {
	meta: {
		type: "layout",
		fixable: "code",
		schema: [],
		messages: {
			unnecessary: "Unnecessary '()' when invoking a constructor in 'new class'.",
			arguments: "Unexpected parameters in 'new class' idiom"
		}
	},

	create(context) {
		const sourceCode = context.getSourceCode();

		return {
			NewExpression(node) {
				if(node.callee.type === "ClassExpression") {
					const lastToken = sourceCode.getLastToken(node);
					const hasLastParen = lastToken && astUtils.isClosingParenToken(lastToken);
					const hasParens = hasLastParen && astUtils.isOpeningParenToken(sourceCode.getTokenBefore(lastToken));

					if(node.arguments.length > 0) {
						context.report({
							node,
							messageId: "arguments"
						});
					} else if(hasParens) {
						context.report({
							node,
							messageId: "unnecessary",
							fix: fixer => fixer.removeRange([node.callee.end, node.end])
						});
					}
				}
			}
		};
	}
};