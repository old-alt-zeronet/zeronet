module.exports = {
	meta: {
		type: "layout",
		fixable: "whitespace",
		schema: []
	},

	create(context) {
		const sourceCode = context.getSourceCode();

		function getFirstNonSpacedToken(left, right, op) {
			const operator = sourceCode.getFirstTokenBetween(left, right, token => token.value === op);
			const prev = sourceCode.getTokenBefore(operator);
			const next = sourceCode.getTokenAfter(operator);

			if(!sourceCode.isSpaceBetweenTokens(prev, operator) || !sourceCode.isSpaceBetweenTokens(operator, next)) {
				return operator;
			}

			return null;
		}

		function report(mainNode, culpritToken) {
			context.report({
				node: mainNode,
				loc: culpritToken.loc.start,
				message: "Operator '{{operator}}' must be spaced.",
				data: {
					operator: culpritToken.value
				},
				fix(fixer) {
					const previousToken = sourceCode.getTokenBefore(culpritToken);
					const afterToken = sourceCode.getTokenAfter(culpritToken);
					let fixString = "";

					if(culpritToken.range[0] - previousToken.range[1] === 0) {
						fixString = " ";
					}

					fixString += culpritToken.value;

					if(afterToken.range[0] - culpritToken.range[1] === 0) {
						fixString += " ";
					}

					return fixer.replaceText(culpritToken, fixString);
				}
			});
		}

		function reportUnspaced(mainNode, culpritToken) {
			context.report({
				node: mainNode,
				loc: culpritToken.loc.start,
				message: "There must be no spaces around '='' in function declaration",
				fix(fixer) {
					const previousToken = sourceCode.getTokenBefore(culpritToken);
					const afterToken = sourceCode.getTokenAfter(culpritToken);

					return [
						fixer.removeRange([previousToken.end, culpritToken.start]),
						fixer.removeRange([culpritToken.end, afterToken.start])
					];
				}
			});
		}

		/**
		 * Check if the node is binary then report
		 * @param {ASTNode} node node to evaluate
		 * @returns {void}
		 * @private
		 */
		function checkBinary(node) {
			const leftNode = (node.left.typeAnnotation) ? node.left.typeAnnotation : node.left;
			const rightNode = node.right;

			// Force = without spaces in function definition
			if(
				node.type === "AssignmentPattern" &&
				node.parent.type === "FunctionExpression"
			) {
				const operator = sourceCode.getFirstTokenBetween(leftNode, rightNode, token => token.value === "=");
				const prev = sourceCode.getTokenBefore(operator);
				const next = sourceCode.getTokenAfter(operator);

				if(sourceCode.isSpaceBetweenTokens(prev, operator) || sourceCode.isSpaceBetweenTokens(operator, next)) {
					reportUnspaced(node, operator);
				}
			} else {
				// Search for = in AssignmentPattern nodes
				const operator = node.operator || "=";

				const nonSpacedNode = getFirstNonSpacedToken(leftNode, rightNode, operator);

				if(nonSpacedNode) {
					report(node, nonSpacedNode);
				}
			}
		}

		/**
		 * Check if the node is conditional
		 * @param {ASTNode} node node to evaluate
		 * @returns {void}
		 * @private
		 */
		function checkConditional(node) {
			const nonSpacedConsequesntNode = getFirstNonSpacedToken(node.test, node.consequent, "?");
			const nonSpacedAlternateNode = getFirstNonSpacedToken(node.consequent, node.alternate, ":");

			if(nonSpacedConsequesntNode) {
				report(node, nonSpacedConsequesntNode);
			} else if(nonSpacedAlternateNode) {
				report(node, nonSpacedAlternateNode);
			}
		}

		/**
		 * Check if the node is a variable
		 * @param {ASTNode} node node to evaluate
		 * @returns {void}
		 * @private
		 */
		function checkVar(node) {
			const leftNode = (node.id.typeAnnotation) ? node.id.typeAnnotation : node.id;
			const rightNode = node.init;

			if(rightNode) {
				const nonSpacedNode = getFirstNonSpacedToken(leftNode, rightNode, "=");

				if(nonSpacedNode) {
					report(node, nonSpacedNode);
				}
			}
		}

		return {
			AssignmentExpression: checkBinary,
			AssignmentPattern: checkBinary,
			BinaryExpression: checkBinary,
			LogicalExpression: checkBinary,
			ConditionalExpression: checkConditional,
			VariableDeclarator: checkVar
		};

	}
};