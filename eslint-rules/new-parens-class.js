const astUtils = require("eslint/lib/rules/utils/ast-utils");

module.exports = {
	meta: {
		type: "layout",
		fixable: "code",
		schema: [],
		messages: {
			missing: "Missing '()' invoking a constructor."
		}
	},

	create(context) {
		const sourceCode = context.getSourceCode();

		return {
			NewExpression(node) {
				if(node.arguments.length !== 0) {
					return; // If there are arguments, there have to be parens
				}

				const lastToken = sourceCode.getLastToken(node);
				const hasLastParen = lastToken && astUtils.isClosingParenToken(lastToken);
				const hasParens = hasLastParen && astUtils.isOpeningParenToken(sourceCode.getTokenBefore(lastToken));

				// Allow `new class` idiom
				if(!hasParens && node.callee.type !== "ClassExpression") {
					context.report({
						node,
						messageId: "missing",
						fix: fixer => fixer.insertTextAfter(node, "()")
					});
				}
			}
		};
	}
};