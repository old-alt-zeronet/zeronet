import http from "http";
import url from "url";
import Config from "../../config";

export default class HttpServer {
	constructor([ip, port], peerServer, workerManager, siteManager, blobManager) {
		this.ip = ip;
		this.port = port;
		this.peerServer = peerServer;
		this.workerManager = workerManager;
		this.siteManager = siteManager;
		this.blobManager = blobManager;
		this.server = http.createServer(this.handleRequest.bind(this));
		this.server.on("listening", () => {
			Config.logger.info(`HttpServer: Server listening on ${ip}:${port}`);
		});
	}
	serve() {
		this.server.listen(this.port, this.ip);
	}

	async handleRequest(req, res) {
		Config.logger.info(`HttpServer: ${req.method} ${req.url}`);
		if(req.url.startsWith("/ZeroNet-Internal/")) {
			// Handle as a command
			const cmd = req.url.split("/")[2];
			const key = `command${cmd[0].toUpperCase()}${cmd.substr(1)}`;
			if(!this[key]) {
				res.writeHead(404);
				res.end("Unknown command");
				return;
			}
			for await(const val of this[key](res)) {
				res.write(val);
			}
			res.end();
		} else if(req.url === "/") {
			// Homepage
			res.writeHead(302, {
				Location: `/${Config.zeronet.homepage}`
			});
			res.end();
		} else {
			// A site most likely
			const reqUrl = url.parse(req.url);
			const siteAddress = reqUrl.pathname.split("/")[1];
			const filePath = reqUrl.pathname.split("/").slice(2).join("/");
			// Download the required file
			const site = this.siteManager.get(siteAddress);
			try {
				await site.readFile("content.json");
			} catch(e) {
				// First download
				site.downloadBlind();
			}
			try {
				const f = await Promise.race([
					site.needFile(filePath),
					new Promise((_, reject) => {
						setTimeout(() => reject(new Error("Timeout")), 10 * 60 * 1000);
					})
				]);
				if(f === true) {
					res.end(await site.readFile(filePath));
				} else if(f === "directory") {
					// Looks like we're actually looking for index.html
					if(reqUrl.pathname.endsWith("/")) {
						const indexPath = `${filePath}index.html`;
						await Promise.race([
							site.needFile(indexPath),
							new Promise((_, reject) => {
								setTimeout(() => reject(new Error("Timeout")), 10 * 60 * 1000);
							})
						]);
						res.end(await site.readFile(indexPath));
					} else {
						// Fix relative URLs
						res.writeHead(302, {
							Location: `${req.url}/`
						});
						res.end();
					}
				}
			} catch(e) {
				res.writeHead(404, {
					"Content-Type": "text/html, charset=utf-8"
				});
				res.end(`File not found. Reason: ${e}`);
			}
		}
	}
}