import install from "../plugins/install";
import Config from "../config";
import HttpServer from "./httpserver";
import WorkerManager from "../core/peer/client/worker/manager";
import BlobManager from "../core/blob/manager";
import UserManager from "../core/user/manager";
import PeerServer from "../core/peer/server";
import SiteManager from "../core/site/manager";
import Discovery from "../core/discovery";

(async () => {
	// Install plugins
	await install();

	// Create user manager
	const userManager = new UserManager(); // eslint-disable-line no-unused-vars

	// Create discovery service
	const discovery = new Discovery();

	// Create worker manager
	const workerManager = new WorkerManager(discovery);

	// Start peer server
	const peerServer = new PeerServer(
		Config.zeronet.peerServer.port,
		workerManager,
		discovery
	);
	peerServer.serve();

	// Create blob manager
	const blobManager = new BlobManager(workerManager, peerServer);
	await blobManager.loadDB();
	// Initialize GC
	const gc = async () => {
		await blobManager.gc();
		setTimeout(gc, 60 * 1000);
	};
	setTimeout(gc, 60 * 1000);

	// Create site manager
	const siteManager = new SiteManager(
		workerManager,
		blobManager,
		peerServer
	);

	// Start HTTP server
	const {ip, port} = Config.zeronet.ui;
	const server = new HttpServer(
		[ip, port],
		peerServer,
		workerManager,
		siteManager,
		blobManager
	);
	server.serve();
})();