module.exports = {
	testMatch: [
		"**/test.mjs",
		"**/test.js"
	],
	moduleFileExtensions: ["js", "json", "mjs"],
	transform: {
		"\\.m?js$": "babel-jest"
	},

	collectCoverage: true,

	testEnvironment: "node"
};