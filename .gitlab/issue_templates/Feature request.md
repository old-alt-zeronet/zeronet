## Why are you creating this issue?

- [ ] I found a bug *(e.g.: ZeroMe works on the official implementation but not here)*
- [ ] I want to discuss a plugin/feature idea *(e.g.: PeerMessage plugin)*
- [x] I want to request a feature *(e.g.: Private sites support)*
- [ ] I have a question about ZeroNet core *(e.g.: Why are blobs hardlinked?)*

## Feature details

*e.g.: It'd be cool if sites could be encrypted with a password.*