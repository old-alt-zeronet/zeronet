## Why are you creating this issue?

- [x] I found a bug *(e.g.: ZeroMe works on the official implementation but not here)*
- [ ] I want to discuss a plugin/feature idea *(e.g.: PeerMessage plugin)*
- [ ] I want to request a feature *(e.g.: Private sites support)*
- [ ] I have a question about ZeroNet core *(e.g.: Why are blobs hardlinked?)*

## Bug description

*e.g.: ZeroMe works on the official implementation but not here. I don't get any error, however, I can't comment any posts. There is nothing in the console (neither in browser console nor in Alt-ZeroNet console).*

## Steps to reproduce

*e.g.:*

1. *Open ZeroMe at me.zeronetwork.bit.*
2. *Download any hub that contains posts (Blue Hub doesn't work for sure).*
3. *Try to post a comment.*

## What's the expected behavior?

*Leave this empty if it's obvious.*