## Why are you creating this issue?

- [ ] I found a bug *(e.g.: ZeroMe works on the official implementation but not here)*
- [ ] I want to discuss a plugin/feature idea *(e.g.: PeerMessage plugin)*
- [ ] I want to request a feature *(e.g.: Private sites support)*
- [x] I have a question about ZeroNet core *(e.g.: Why are blobs hardlinked?)*

## Question

*e.g.: I've noticed that changing a blob in data/blobs/ directory changes the content in data/sites/ instantly. I stopped Alt-ZeroNet to make sure that it's not handled programmatically. The files were still linked. Some utilities report that most inodes have 2 references (though content.json's have only one). Thus, it looks like some files in data/blobs/ and data/sites/ are hardlinked. What's the reason of such choice?*