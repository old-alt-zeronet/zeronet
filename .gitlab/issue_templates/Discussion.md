## Why are you creating this issue?

- [ ] I found a bug *(e.g.: ZeroMe works on the official implementation but not here)*
- [x] I want to discuss a plugin/feature idea *(e.g.: PeerMessage plugin)*
- [ ] I want to request a feature *(e.g.: Private sites support)*
- [ ] I have a question about ZeroNet core *(e.g.: Why are blobs hardlinked?)*

## Idea

*e.g.: I'm sure that real-time messaging is important for ZeroNet. Check a simple specification I've made below. I'm open for any suggestions.*