import fs from "fs";
import path from "path";
import winston from "winston";
import uri2path from "file-uri-to-path";

const logger = winston.createLogger({
	format: winston.format.combine(
		winston.format.colorize(),
		winston.format.simple()
	),
	transports: [
		new winston.transports.Console()
	]
});

const DEFAULT_TRACKERS = [
	"zero://boot3rdez4rzn36x.onion:15441",
	"zero://zero.booth.moe#f36ca555bee6ba216b14d10f38c16f7769ff064e0e37d887603548cc2e64191d:443",
	"udp://tracker.coppersurfer.tk:6969",
	"udp://tracker.port443.xyz:6969",
	"udp://104.238.198.186:8000",
	"http://tracker2.itzmx.com:6961/announce",
	"http://open.acgnxtracker.com:80/announce",
	"http://open.trackerlist.xyz:80/announce",
	"zero://2602:ffc5::c5b2:5360:26312"
];

const DEFAULT_ZERONET_CONFIG = {
	ui: {
		ip: null,
		port: 2370
	},
	peerServer: {
		port: 45321
	},
	plugins: {},
	homepage: "1HeLLo4uzjaLetFx6NH3PMwFP3qbRbTf3D"
};


export default new class Config {
	constructor() {
		this.logger = logger;
		this.root = path.dirname(uri2path(import.meta.url));
		// Create & init data directory
		this.dataDirectory = path.join(this.root, "data");
		try {
			fs.mkdirSync(this.dataDirectory);
			this.logger.info("data directory did not exist, created");
		} catch(e) {
			this.logger.info("data directory is ok");
		}
		// Load tracker list
		try {
			this.trackers = JSON.parse(fs.readFileSync(path.join(this.dataDirectory, "trackers.json"), "utf8"));
			this.logger.info("trackers.json file is ok");
		} catch(e) {
			this.logger.info("trackers.json file is malformed or doesn't exist, created");
			this.trackers = DEFAULT_TRACKERS;
			this.saveTrackers();
		}
		// Load some ZeroNet-specific info
		try {
			this.zeronet = JSON.parse(fs.readFileSync(path.join(this.dataDirectory, "zeronet.json"), "utf8"));
			this.logger.info("zeronet.json file is ok");
		} catch(e) {
			this.logger.info("zeronet.json file is malformed or doesn't exist, created");
			this.zeronet = DEFAULT_ZERONET_CONFIG;
			this.saveZeroNetConfig();
		}
		// Load site list & info
		try {
			this.sites = JSON.parse(fs.readFileSync(path.join(this.dataDirectory, "sites.json"), "utf8"));
			this.logger.info("sites.json file is ok");
		} catch(e) {
			this.logger.info("sites.json file is malformed or doesn't exist, created");
			this.sites = {};
			this.saveSites();
		}
		// Create sites directory if required
		try {
			fs.mkdirSync(path.join(this.dataDirectory, "sites"));
			this.logger.info("data/sites directory did not exist, created");
		} catch(e) {
			this.logger.info("data/sites directory is ok");
		}
		// Create blobs directory if required
		try {
			fs.mkdirSync(path.join(this.dataDirectory, "blobs"));
			this.logger.info("data/blobs directory did not exist, created");
		} catch(e) {
			this.logger.info("data/blobs directory is ok");
		}
		// Some info
		this.version = "0.7.0";
		this.rev = 4110;
	}

	saveTrackers() {
		fs.writeFileSync(path.join(this.dataDirectory, "trackers.json"), JSON.stringify(this.trackers, null, "\t"));
	}

	saveZeroNetConfig() {
		fs.writeFileSync(path.join(this.dataDirectory, "zeronet.json"), JSON.stringify(this.zeronet, null, "\t"));
	}

	saveSites() {
		fs.writeFileSync(path.join(this.dataDirectory, "sites.json"), JSON.stringify(this.sites, null, "\t"));
	}
};