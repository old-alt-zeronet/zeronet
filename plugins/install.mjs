// This file extension (.js) is not a typo. We want to use require() here (as we
// have dynamic paths), and require() is only available in non-ESM files.

import fs from "fs";
import path from "path";
import Config from "../config";
import URL from "url";

export default async function install() {
	Config.logger.info("Loading plugins");
	// List plugins
	for(const name of fs.readdirSync(path.join(Config.root, "plugins"))) {
		const pluginRoot = path.join(Config.root, "plugins", name);
		if(fs.statSync(pluginRoot).isDirectory()) {
			// Check whether this plugin is disabled
			const camelCaseName = name.replace(/-([a-z])/g, (_, c) => c.toUpperCase());
			if(Config.zeronet.plugins[camelCaseName] === false) {
				Config.logger.info(`Plugin ${name} is disabled`);
				continue;
			}

			// Load plugin
			try {
				await import(URL.pathToFileURL(path.join(pluginRoot, "index.mjs")));
				Config.logger.info(`Plugin ${name} loaded successfully`);
			} catch(e) {
				Config.logger.error(`Error when loading plugin ${name}: ${e}`);
			}
		}
	}
}