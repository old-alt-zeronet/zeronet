export default function extend(cls) {
	cls = cls.prototype;
	const superCls = cls.__proto__;

	// From:
	// +---------------------+
	// | cls                 |
	// +=====================+
	// | method1a            |
	// | method2             |
	// | +-----------------+ |
	// | | superCls        | |
	// | +=================+ |
	// | | method1b        | |
	// | | method3         | |
	// | | +-------------+ | |
	// | | | __proto__   | | |
	// | | +=============+ | |
	// | | | method4     | | |
	// | | | method5     | | |
	// | | +-------------+ | |
	// | +-----------------+ |
	// +---------------------+
	// To:
	// +---------------------+
	// | superCls            |
	// +=====================+
	// | method1a            |
	// | method2             |
	// | +-----------------+ |
	// | | __proto__       | |
	// | +=================+ |
	// | | method1b        | |
	// | | method3         | |
	// | | +-------------+ | |
	// | | | __proto__   | | |
	// | | +=============+ | |
	// | | | method4     | | |
	// | | | method5     | | |
	// | | +-------------+ | |
	// | +-----------------+ |
	// +---------------------+

	const newSuperClsProto = Object.create(superCls.__proto__);
	for(const key of Object.getOwnPropertyNames(cls)) {
		// Save replaced method
		newSuperClsProto[key] = superCls[key];
		// Actually replace method
		superCls[key] = cls[key];
	}
	// Set __proto__
	superCls.__proto__ = newSuperClsProto;
	// Set __proto__ of cls. This fixes "super" in plugin methods.
	//
	// If you're seeking for an explanation, here it is: in JavaScript,
	// "super.x" is the same as "arguments.callee.[[HomeObject]].__proto__.x".
	// Here, "arguments.callee" is the current method, [[HomeObject]] is a
	// special property that holds the *class* that contains this method. This
	// means that methods *know* where their are hold; actually, [[HomeObject]]
	// is the *only* property that contains this information, and "super" is the
	// *only* part of JavaScript that uses it somehow.
	//
	// Back to the topic. We want "super.x" to work. We can't change
	// [[HomeObject]], so we have to change __proto__. Replacing __proto__ of
	// "arguments.callee.[[HomeObject]]" is enough to make "super" work the way
	// we want it to.
	cls.__proto__ = newSuperClsProto;
}