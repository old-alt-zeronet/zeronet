import HttpServer from "../../webui/httpserver";
import extend from "..";

extend(class extends HttpServer {
	async *commandStats(res) {
		res.writeHead(200, {
			"Content-Type": "text/html"
		});

		yield "<h2>Locked files</h2>\n";
		yield "<pre>";
		yield "Path\n";
		for(const filePath of Object.keys(this.blobManager.fileMutexesReason)) {
			yield `...${filePath.slice(-100).padEnd(100)}  ${this.blobManager.fileMutexesReason[filePath]}\n`;
		}
		yield "</pre>\n";

		yield "<h2>Sites</h2>\n";
		yield "<pre>\n";
		yield "Address                            DB status  Binding blobs  Bind calls  Loading content  Writing content  Write calls  DB updating files  DB update calls\n";
		for(const address of Object.keys(this.siteManager.sites)) {
			const site = this.siteManager.sites[address];
			const dbInfo = await site.db.getInfo();
			yield address.padEnd(35);
			if(site.db.error !== null) {
				yield "error      ";
			} else if(!dbInfo.schema) {
				yield "no schema  ";
			} else if(dbInfo.openMutex) {
				yield "locked     ";
			} else if(!dbInfo.isOpen) {
				yield "closed     ";
			} else if(dbInfo.ensureLock === 1) {
				yield "writing    ";
			} else if(dbInfo.ensureLock === 2) {
				yield "ensuring   ";
			} else {
				yield "idle       ";
			}
			yield site.cntBinding.toString().padEnd(15);
			yield site.bindCalls.toString().padEnd(12);
			yield site.cntContentLoading.toString().padEnd(17);
			yield site.cntContentWriting.toString().padEnd(17);
			yield site.contentWrites.toString().padEnd(13);
			yield site.db.cntUpdating.toString().padEnd(19);
			yield site.db.updateCalls.toString();
			yield "\n";
			if(site.db.error !== null) {
				yield "  DB error: ";
				yield site.db.error.toString();
				yield "\n";
			}
			for(const task of dbInfo.currentTasks) {
				yield `  DB: ${task.status.padEnd(10)}   ${task.timeToStart.toString().padStart(5)}ms ${task.filePath}\n`;
			}
		}
		yield "</pre>\n";

		yield "<h2>Peers</h2>\n";
		yield "<pre>\n";
		yield "   Address                  Rating  Ready  Requests  Failed tasks  Current task\n";
		for(const worker of this.workerManager.workers) {
			yield worker.peer.transport.isIncoming ? "-> " : "<- ";
			yield worker.peer.transport.addr.padEnd(25);
			yield worker.peer.bad.toString().padEnd(8);
			yield worker.peer.isReady ? "Yes    " : (worker.peer.transport.isConnected ? "No     " : "Con... ");
			yield worker.peer.reqId.toString().padEnd(10);
			yield worker.failedTasks.toString().padEnd(14);

			if(worker.currentTask) {
				const task = {...worker.currentTask};
				if(task.site) {
					task.site = task.site.address;
				}
				yield JSON.stringify(task);
			} else {
				yield "none";
			}
			yield "\n";

			// Hosted stuff
			for(const site of worker.sites) {
				yield `     Site: ${site.address}\n`;
			}
			for(const blob of worker.blobs) {
				yield `     Blob: ${blob}\n`;
			}
		}
		yield "</pre>\n";

		yield "<h2>Queued tasks</h2>\n";
		yield "<pre>\n";
		yield "Priority  Type           Site             Task\n";
		const tasks = [];
		this.workerManager.tasks.forEach(task => {
			tasks.push(task);
		});
		for(let task of tasks) {
			task = {...task};
			const priority = task.priority;
			const type = task.type;
			const site = (
				task.site
					? `${task.site.address.substr(0, 12)}...`
					: "none"
			);
			delete task.priority;
			delete task.type;
			delete task.site;
			yield priority.toString().padEnd(10);
			yield type.padEnd(15);
			yield site.padEnd(17);
			yield JSON.stringify(task);
			yield "\n";
		}
		yield "</pre>";
	}
});