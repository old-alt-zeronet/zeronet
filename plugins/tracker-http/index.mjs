import Discovery from "../../core/discovery";
import extend from "..";
import crypto from "crypto";
import bencode from "bencode";
import qs from "querystring";
import request from "request";

extend(class extends Discovery {
	async announceSingleTracker(id, tracker, peerServer, fast) {
		if(["http", "https"].indexOf(tracker.split("://")[0]) > -1) {
			return await announceHttp(id, tracker, peerServer, fast);
		} else {
			return await super.announceSingleTracker(id, tracker, peerServer, fast);
		}
	}
});

async function announceHttp(id, tracker, peerServer, fast) {
	const sha1 = escape(
		crypto
			.createHash("sha1")
			.update(id)
			.digest()
			.toString("binary")
	);
	const params = qs.stringify({
		peer_id: peerServer.peerId, // eslint-disable-line camelcase
		port: peerServer.port,
		uploaded: 0,
		downloaded: 0,
		left: 431102370,
		compact: 1,
		numwant: fast ? 50 : 25,
		event: "started"
	});
	// We don't move sha1 to params because qs.stringify uses encodeURIComponent
	// instead of escape, which gives wrong results on binary data
	const url = `${tracker}?info_hash=${sha1}&${params}`;

	const body = await new Promise((resolve, reject) => {
		request({
			url,
			timeout: 10000,
			encoding: null
		}, (e, _, body) => { // eslint-disable-line no-shadow
			if(e) {
				reject(e);
			} else {
				resolve(body);
			}
		});
	});

	const data = bencode.decode(body).peers;
	const peers = [];
	for(let i = 0; i < data.length; i += 6) {
		const ip = Array.from(data.slice(i, i + 4)).join(".");
		const port = (data[i + 4] << 8) | data[i + 5];
		peers.push(`${ip}:${port}`);
	}
	return peers;
}