import Discovery from "../../core/discovery";
import extend from "..";
import crypto from "crypto";
import dgram from "dgram";

extend(class extends Discovery {
	async announceSingleTracker(id, tracker, peerServer, fast) {
		if(tracker.startsWith("udp://")) {
			return await announceUdp(id, tracker, peerServer, fast);
		} else {
			return await super.announceSingleTracker(id, tracker, peerServer, fast);
		}
	}
});

async function announceUdp(id, tracker, peerServer, fast) {
	const [ip, port] = tracker.replace("udp://", "").split(":");

	const transactionId = crypto.randomBytes(4);

	const client = dgram.createSocket("udp4");

	const sendAndWait = async msg => {
		client.send(msg, +port, ip);
		const interval = setInterval(() => {
			client.send(msg, +port, ip);
		}, 1000);
		return await new Promise(resolve => {
			const handler = (data, rinfo) => {
				if(rinfo.address === ip && rinfo.port === +port) {
					clearInterval(interval);
					client.off("message", handler);
					resolve(data);
				}
			};
			client.on("message", handler);
		});
	};

	// Connect
	const connData = await sendAndWait(Buffer.concat([
		Buffer.from([0x00, 0x00, 0x04, 0x17, 0x27, 0x10, 0x19, 0x80]),
		Buffer.from([0, 0, 0, 0]), // connect
		transactionId
	]));
	if(connData.length < 16) {
		client.close();
		throw new Error("Connection response must be at least 16 bytes long");
	}
	if(!connData.slice(0, 4).equals(Buffer.from([0, 0, 0, 0]))) {
		client.close();
		throw new Error("Connection response action != 0");
	}
	if(!connData.slice(4, 8).equals(transactionId)) {
		client.close();
		throw new Error("Connection response contains wrong transaction ID");
	}
	const connId = connData.slice(8, 16);

	// Announce
	const announceData = await sendAndWait(Buffer.concat([
		connId,
		Buffer.from([0, 0, 0, 1]), // announce
		transactionId,
		crypto.createHash("sha1").update(id).digest(),
		Buffer.from(peerServer.peerId, "binary"),
		Buffer.from([0, 0, 0, 0, 0, 0, 0, 0]), // downloaded
		Buffer.from([0x00, 0x00, 0x00, 0x00, 0x19, 0xb2, 0x19, 0xa2]), // left
		Buffer.from([0, 0, 0, 0, 0, 0, 0, 0]), // uploaded
		Buffer.from([0, 0, 0, 2]), // event: started
		Buffer.from([0, 0, 0, 0]), // ip: autodetect
		Buffer.from([0, 0, 0, 0]), // key (optional)
		Buffer.from([0, 0, 0, fast ? 50 : 25]), // numwant
		Buffer.from([peerServer.port >> 8, peerServer.port & 0xff])
	]));
	if(announceData.length < 20) {
		client.close();
		throw new Error("Announce response must be at least 20 bytes long");
	}
	if(!announceData.slice(0, 4).equals(Buffer.from([0, 0, 0, 1]))) {
		client.close();
		throw new Error("Announce response action != 1");
	}
	if(!announceData.slice(4, 8).equals(transactionId)) {
		client.close();
		throw new Error("Announce response contains wrong transaction ID");
	}
	client.close(); // thanks!

	const peers = [];
	for(let i = 20; i < announceData.length; i += 6) {
		const peerIp = Array.from(announceData.slice(i, i + 4)).join(".");
		const peerPort = (announceData[i + 4] << 8) | announceData[i + 5];
		peers.push(`${peerIp}:${peerPort}`);
	}
	return peers;
}