export default class User {
	constructor(masterAddress, {certs, master_seed: masterSeed, settings, sites}) {
		this.masterAddress = masterAddress;
		this.certs = certs;
		this.masterSeed = masterSeed;
		this.settings = settings || {};
		this.sites = sites;
	}
}