import fs from "fs";
import path from "path";
import Config from "../../config";
import User from ".";


export default class UserManager {
	constructor() {
		this.usersPath = path.join(Config.dataDirectory, "users.json");
		// Load & parse users
		let users;
		try {
			users = fs.readFileSync(this.usersPath);
		} catch(e) {
			// No problem
			users = "{}";
		}
		try {
			users = JSON.parse(users);
		} catch(e) {
			Config.logger.error("data/users.json is malformed");
			process.exit(1);
		}
		// Create User objects
		this.users = {};
		for(const masterAddress of Object.keys(users)) {
			this.users[masterAddress] = new User(masterAddress, users[masterAddress]);
		}
	}
}