import {parentPort} from "worker_threads";


const reCache = {};

parentPort.on("message", ([task, re, text]) => {
	if(!reCache[re]) {
		reCache[re] = new RegExp(re);
	}

	if(task === "test") {
		parentPort.postMessage(reCache[re].test(text));
	}
});