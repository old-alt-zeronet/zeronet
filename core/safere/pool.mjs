import {Worker} from "worker_threads";
import path from "path";
import uri2path from "file-uri-to-path";


class SafeReWorker {
	constructor(queue) {
		this.queue = queue;
		this.isWorking = false;
		this.worker = new Worker(
			path.join(path.dirname(uri2path(import.meta.url)), "thread.mjs")
		);
	}

	async _work(...args) {
		return await new Promise(resolve => {
			this.worker.once("message", resolve);
			this.worker.postMessage(args);
		});
	}

	async workImmediately(...args) {
		this.isWorking = true;
		const res = await this._work(...args);
		this.isWorking = false;
		this.checkQueue();
		return res;
	}

	async checkQueue() {
		this.isWorking = true;
		while(this.queue.length > 0) {
			const {args, resolve} = this.queue.shift();
			resolve(await this._work(...args));
		}
		this.isWorking = false;
	}
}


const workers = [];
const queue = [];
for(let i = 0; i < 10; i++) {
	workers.push(new SafeReWorker(queue));
}


export default async function work(...args) {
	const freeWorker = workers.find(w => !w.isWorking);
	if(freeWorker) {
		return await freeWorker.workImmediately(...args);
	} else {
		return await new Promise(resolve => {
			queue.push({args, resolve});
		});
	}
}