import fs from "fs";
import path from "path";
import util from "util";
import sqlite from "sqlite-async";
import Config from "../../config";
import _Mutex from "await-mutex";
const Mutex = _Mutex.default; // Someone doesn't know how to use ESM modules


export default class BlobManager {
	constructor(workerManager, peerServer) {
		this.workerManager = workerManager;
		this.peerServer = peerServer;
		this.db = null;
		this.mutexes = {};
		this.mutexesRelease = {};
		this.fileMutexes = {};
		this.fileMutexesRelease = {};
		this.fileMutexesReason = {};
	}
	async loadDB() {
		// Load database
		const dbPath = path.join(Config.dataDirectory, "blobs.db");
		this.db = await sqlite.open(dbPath);
		// Make sure the database is valid
		let tableCount;
		try {
			tableCount = (await this.db.get("SELECT COUNT(*) AS cnt FROM sqlite_master")).cnt;
			Config.logger.info("data/blobs.db is ok");
		} catch(e) {
			// Oopsie, it's malformed
			Config.logger.error("data/blobs.db is malformed, recreating");
			await this.db.close();
			// Delete database
			await util.promisify(fs.unlink)(dbPath);
			// Try again
			this.db = await sqlite.open(dbPath);
			tableCount = 0; // Makes sense, right?
		}
		// Check that the tables we need exist
		if(tableCount === 0) {
			// Looks like a clean db
			Config.logger.info("data/blobs.db is clean, creating tables");
			await this.db.exec(`
				CREATE TABLE frozen_blobs(id TEXT, external_path TEXT);
				CREATE INDEX frozen_blob_id ON frozen_blobs(id);
				CREATE UNIQUE INDEX frozen_blob_path
					ON frozen_blobs(external_path)
			`);
			// Now load table content from data/ directory
			await this.fillDB();
		}
	}
	async fillDB() {
		Config.logger.info("Filling data/blobs.db from data directory");
		Config.logger.info("  This might take a while, please wait...");

		// Generate inode -> blob ID mapping
		const blobs = {};
		const blobsDirectory = path.join(Config.dataDirectory, "blobs");
		for(const id of await util.promisify(fs.readdir)(blobsDirectory)) {
			const blobPath = path.join(blobsDirectory, id);
			const stat = await util.promisify(fs.stat)(
				blobPath,
				{bigint: true}
			);
			blobs[stat.ino] = id;
		}

		const stmt = await this.db.prepare(
			"INSERT INTO frozen_blobs VALUES (?, ?)"
		);
		let cnt = 0;

		const walk = async filePath => {
			if(filePath === blobsDirectory) {
				// Skip
				return;
			}
			const stat = await util.promisify(fs.stat)(
				filePath,
				{bigint: true}
			);
			if(stat.isDirectory()) {
				// Run recursively
				const children = await util.promisify(fs.readdir)(filePath);
				await Promise.all(
					children.map(name => walk(path.join(filePath, name)))
				);
			} else if(stat.isFile()) {
				if(blobs[stat.ino]) {
					// Yay! It's a blob
					await stmt.run([blobs[stat.ino], filePath]);
					cnt++;
				}
			}
		};
		await walk(Config.dataDirectory);

		await stmt.finalize();
		Config.logger.info(`Done (inserted ${cnt} row(s))`);
	}

	async withDb(cmd, query, params) {
		const stmt = await this.db.prepare(query);
		const res = await stmt[cmd](params);
		await stmt.finalize();
		return res;
	}


	// Enter critical section for a blob
	async acquire(id) {
		if(!this.mutexes[id]) {
			this.mutexes[id] = new Mutex();
		}
		this.mutexesRelease[id] = await this.mutexes[id].lock();
	}
	// Exit critical section for a blob
	release(id) {
		const unlock = this.mutexesRelease[id];
		delete this.mutexesRelease[id];
		unlock();
	}

	// Enter critical section for a file
	async acquireFile(filePath, reason) {
		if(!this.fileMutexes[filePath]) {
			this.fileMutexes[filePath] = new Mutex();
		}
		this.fileMutexesRelease[filePath] = await this.fileMutexes[filePath].lock();
		this.fileMutexesReason[filePath] = reason;
	}
	// Exit critical section for a file
	releaseFile(filePath) {
		const unlock = this.fileMutexesRelease[filePath];
		delete this.fileMutexesRelease[filePath];
		delete this.fileMutexesReason[filePath];
		unlock();
	}


	// Downloads a blob by ID and saves it to blob filesystem
	async download(id, site=null, filePath=null) {
		// Download blob
		if(site !== null) {
			await this.workerManager.ensurePeersForSite(site, this.peerServer);
		}
		const blob = await this.workerManager.run({
			type: "getBlob",
			blob: id,
			site,
			path: filePath
		}, filePath === null ? 100 : 100 - filePath.length);

		// Save the blob
		const dest = path.join(Config.dataDirectory, "blobs", id);
		await util.promisify(fs.writeFile)(dest, blob);
	}

	// Downloads a blob if required
	async ensure(id, site=null, filePath=null) {
		const blobPath = path.join(Config.dataDirectory, "blobs", id);
		try {
			await util.promisify(fs.stat)(blobPath);
		} catch(e) {
			// Download the blob
			await this.download(id, site, filePath);
		}
	}


	// Binds the blob from blob filesystem to external filesystem
	async bindFrozen(id, filePath) {
		// Create parent directories if required
		await util.promisify(fs.mkdir)(path.dirname(filePath), {
			recursive: true
		});
		// Delete old file if it exists
		try {
			await util.promisify(fs.unlink)(filePath);
		// eslint-disable-next-line no-empty
		} catch(e) {
		}
		// Create hardlink
		const blobPath = path.join(Config.dataDirectory, "blobs", id);
		await util.promisify(fs.link)(blobPath, filePath);
		// Make it read-only to stop user from breaking our blob filesystem
		await util.promisify(fs.chmod)(filePath, 0o500);
		// Add to DB
		await this.withDb(
			"run",
			"INSERT OR REPLACE INTO frozen_blobs VALUES (?, ?)",
			[id, filePath]
		);
	}

	// Copies the blob from blob filesystem to external filesystem
	async bindUnfrozen(id, filePath) {
		// Create parent directories if required
		await util.promisify(fs.mkdir)(path.dirname(filePath), {
			recursive: true
		});
		// Delete old file if it exists
		try {
			await util.promisify(fs.unlink)(filePath);
		// eslint-disable-next-line no-empty
		} catch(e) {
		}
		// Find the blob path
		const blobPath = path.join(Config.dataDirectory, "blobs", id);
		// Check whether the blob is blob filesystem is used, i.e. whether it's
		// referenced in frozen_blobs table somehow
		const isReferenced = !!(await this.withDb(
			"get",
			"SELECT * FROM frozen_blobs WHERE id = ?",
			[id]
		));
		if(isReferenced) {
			// Kk, it's used. This means that we can't just move the blob, we
			// have to copy it. This might take a while :(
			await util.promisify(fs.copyFile)(blobPath, filePath);
		} else {
			// Yay! This blob is *not* used at all, so we can just move it to
			// the place where we need it. Hello, space optimization!
			await util.promisify(fs.rename)(blobPath, filePath);
		}
		// If this file used to hold a frozen blob, delete it from that table
		await this.withDb(
			"run",
			"DELETE FROM frozen_blobs WHERE external_path = ?",
			[filePath]
		);
	}


	async gc() {
		// List through blobs and delete the ones that aren't linked from
		// anywhere
		const blobsDirectory = path.join(Config.dataDirectory, "blobs");
		for(const id of await util.promisify(fs.readdir)(blobsDirectory)) {
			const blobPath = path.join(blobsDirectory, id);
			const stat = await util.promisify(fs.stat)(blobPath);
			// Ensure that this blob isn't referenced from anywhere and that it
			// was downloaded a while ago (to prevent cases when it's removed
			// right after downloading)
			if(stat.nlink === 1 && Date.now() - stat.mtime > 60 * 1000) {
				Config.logger.info(`BlobManager: FS GC: Collecting ${id}`);
				await util.promisify(fs.unlink)(blobPath);
			}
		}
		// Remove non-existing/unfrozen files from DB
		for(const blob of await this.db.all("SELECT * FROM frozen_blobs")) {
			const filePath = blob.external_path;
			let reason;
			try {
				const stat = await util.promisify(fs.stat)(filePath);
				if(stat.mode & 0o200) {
					// This file is writable, thus it's unfrozen
					reason = "unfrozen";
				} else {
					continue;
				}
			} catch(e) {
				reason = "non-existent";
			}
			// This file doesn't exist or is unfrozen
			Config.logger.info(`BlobManager: DB GC: Collecting ${filePath} (reason: ${reason})`);
			await this.withDb(
				"run",
				"DELETE FROM frozen_blobs WHERE external_path = ?",
				[filePath]
			);
		}
	}
}