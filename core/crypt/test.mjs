import * as crypt from ".";

test("encodeInteger", () => {
	expect(crypt.encodeInteger(0)).toEqual(Buffer.from([0]));
	expect(crypt.encodeInteger(12)).toEqual(Buffer.from([12]));
	expect(crypt.encodeInteger(255)).toEqual(Buffer.from([253, 255, 0]));
	expect(crypt.encodeInteger(400)).toEqual(Buffer.from([253, 144, 1]));
	expect(crypt.encodeInteger(65280)).toEqual(Buffer.from([253, 0, 255]));
	expect(crypt.encodeInteger(65536)).toEqual(Buffer.from([254, 0, 0, 1, 0]));
	expect(crypt.encodeInteger(4294967196)).toEqual(Buffer.from([254, 156, 255, 255, 255]));
	expect(crypt.encodeInteger(4294967296)).toEqual(Buffer.from([255, 0, 0, 0, 0, 1, 0, 0, 0]));
});

test("hash", () => {
	expect(crypt.hash("Hello, world!")).toEqual(Buffer.from(
		"02d6c0643e40b0db549cbbd7eb47dcab71a59d7017199ebde6b272f28fbbf95f",
		"hex"
	));
	expect(crypt.hash("ZeroNet rulez")).toEqual(Buffer.from(
		"4ef14beef4ff1ce85e4cbe0c1b85d26a3380ca9b0d26edfc9c263e1e350b2266",
		"hex"
	));
});

test("publicKeyToAddress", () => {
	const uncompressedPublicKey = Buffer.from(
		"04" +
		"ea530c67eb725ab97c9ef1ea01dda51518b667a94af8de6a889673129575e375" +
		"2247098cdea002fcd90e0973b24849ddfecc2af6f43bdd682346e81fb09279ab",
		"hex"
	);
	const uncompressedAddress = "14dGM8SBU28o4c2qHsZyfQYNPSqpbSpP8j";
	expect(crypt.publicKeyToAddress(uncompressedPublicKey))
		.toBe(uncompressedAddress);

	const compressedPublicKey = Buffer.from(
		"03ea530c67eb725ab97c9ef1ea01dda51518b667a94af8de6a889673129575e375",
		"hex"
	);
	const compressedAddress = "1NvpP7QaWSS4JMNYjGDn821o9m9awXvFDP";
	expect(crypt.publicKeyToAddress(compressedPublicKey))
		.toBe(compressedAddress);
});

test("recover", () => {
	const address = "1EtAtp9Re8HvUZkrdukUd25KjSQXEKLsCh";
	const data = "Hello, world!";
	const signature = (
		"HLG2j0whBTayZKlBwgf/NYu2iX+QOUtZyhctU6C1kvrWNBFWgenjCRywPQU5ee7u" +
		"2Wzq/lI/qddDHi+IE+BzZr0="
	);
	expect(crypt.recover(data, signature)).toBe(address);
});

test("verify", () => {
	const signature = (
		"HLG2j0whBTayZKlBwgf/NYu2iX+QOUtZyhctU6C1kvrWNBFWgenjCRywPQU5ee7u" +
		"2Wzq/lI/qddDHi+IE+BzZr0="
	);

	expect(crypt.verify(
		"Hello, world!",
		signature,
		"1EtAtp9Re8HvUZkrdukUd25KjSQXEKLsCh"
	)).toBe(true);

	expect(crypt.verify(
		"Hello, world!",
		signature,
		"14dGM8SBU28o4c2qHsZyfQYNPSqpbSpP8j"
	)).toBe(false);

	expect(crypt.verify(
		"ZeroNet rulez",
		signature,
		"1EtAtp9Re8HvUZkrdukUd25KjSQXEKLsCh"
	)).toBe(false);

	expect(crypt.verify(
		"Hello, world!",
		(
			"G2/L3HpECJF8jc1Ramor3h6Gqs/0IYPvpm4fGmhHdDyjBd1fUWumoVGSuGXDOvya" +
			"qU4sWIUrc6T+MygyNKyLXgk="
		),
		"1EtAtp9Re8HvUZkrdukUd25KjSQXEKLsCh"
	)).toBe(false);

	expect(() => {
		crypt.verify(
			"Hello, world!",
			(
				"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdef" +
				"ghijklmnopqrstuvwxyz0123456789AB" +
				"CDEFGHIJKLMNOPQRSTUVWXY="
			),
			"1EtAtp9Re8HvUZkrdukUd25KjSQXEKLsCh"
		);
	}).toThrow("couldn't recover public key from signature");

	expect(() => {
		crypt.verify(
			"Hello, world!",
			"123",
			"1EtAtp9Re8HvUZkrdukUd25KjSQXEKLsCh"
		);
	}).toThrow("signature length is invalid");
});