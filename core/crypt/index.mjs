import secp256k1 from "secp256k1";
import crypto from "crypto";
import Ripemd160 from "ripemd160";
import bs58 from "bs58";

export function encodeInteger(x) {
	if(x < 253) {
		return Buffer.from([x]);
	} else if(x < 65536) {
		const buf = Buffer.from([253, 0, 0]);
		buf.writeUInt16LE(x, 1);
		return buf;
	} else if(x < 4294967296) {
		const buf = Buffer.from([254, 0, 0, 0, 0]);
		buf.writeUInt32LE(x, 1);
		return buf;
	} else {
		x = BigInt(x);
		const buf = Buffer.from([255, 0, 0, 0, 0, 0, 0, 0, 0]);
		buf.writeUInt32LE(Number(x & ((1n << 32n) - 1n)), 1);
		buf.writeUInt32LE(Number(x >> 32n), 5);
		return buf;
	}
}

export function hash(message) {
	message = Buffer.concat([
		Buffer.from("\x18Bitcoin Signed Message:\n"),
		encodeInteger(message.length),
		Buffer.from(message)
	]);
	message = crypto.createHash("sha256").update(message).digest();
	message = crypto.createHash("sha256").update(message).digest();
	return message;
}

export function publicKeyToAddress(publicKey) {
	let address = Buffer.concat([
		Buffer.from([0]),
		(new Ripemd160()).update(
			crypto.createHash("sha256").update(publicKey).digest()
		).digest()
	]);
	const checksum = crypto
		.createHash("sha256")
		.update(
			crypto.createHash("sha256").update(address).digest()
		)
		.digest()
		.slice(0, 4);
	address = Buffer.concat([address, checksum]);
	return bs58.encode(address);
}

export function recover(data, signature) {
	// Parse electrum signature
	signature = Buffer.from(signature, "base64");
	const recoveryId = (signature[0] - 27) & 3;
	signature = signature.slice(1);
	// Recover public key
	const publicKey = secp256k1.recover(hash(data), signature, recoveryId, false);
	return publicKeyToAddress(publicKey);
}

export function verify(data, signature, address) {
	return recover(data, signature) === address;
}