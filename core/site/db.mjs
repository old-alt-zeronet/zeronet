import path from "path";
import cluster from "cluster";
import Config from "../../config";


export default class SiteDB {
	constructor(site) {
		this.site = site;
		this.error = null;
		cluster.setupMaster({
			exec: path.join(Config.root, "core", "site", "dbthread.mjs"),
			args: [this.site.resolvePath("")]
		});
		this.worker = cluster.fork();
		this.nextId = 1;
		this.cntUpdating = 0;
		this.updateCalls = 0;
		this.init();
	}

	async init() {
		try {
			if(await this.site.needFile("dbschema.json") !== true) {
				return;
			}
		} catch(e) {
			return;
		}

		await this.ensureSchema();
	}


	async ensureSchema() {
		const absPath = this.site.resolvePath("dbschema.json");
		await this.site.blobManager.acquireFile(absPath, "ensure db schema");
		this.error = await this._cmd("ensureSchema");
		this.site.blobManager.releaseFile(absPath);
	}
	async updateFile(filePath) {
		const absPath = this.site.resolvePath(filePath);
		await this.site.blobManager.acquireFile(absPath, "db update");
		this.cntUpdating++;
		this.updateCalls++;
		await this._cmd("updateFile", {
			filePath,
			sentTime: Date.now()
		});
		this.site.blobManager.releaseFile(absPath);
		this.cntUpdating--;
	}

	async getInfo() {
		return await this._cmd("getInfo");
	}


	// IPC
	async _cmd(cmd, args=null) {
		const id = this.nextId++;
		this.worker.send({
			cmd,
			args,
			id
		});
		return await new Promise(resolve => {
			const handler = message => {
				if(message.to === id) {
					resolve(message.response);
					this.worker.off("message", handler);
				}
			};
			this.worker.on("message", handler);
		});
	}


	async run(query, params) {
		return await this._with("run", query, params);
	}
}