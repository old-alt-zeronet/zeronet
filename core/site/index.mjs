import {EventEmitter} from "events";
import util from "util";
import crypto from "crypto";
import fs from "fs";
import path from "path";
import SiteStorage from "./storage";
import SiteDB from "./db";
import Config from "../../config";
import {verifyContent, verifyUserContentSignatures, verifyContentSignatures} from "./sign";


export default class Site extends EventEmitter {
	constructor(address, workerManager, blobManager, peerServer) {
		super();
		this.setMaxListeners(1000);
		this.workerManager = workerManager;
		this.blobManager = blobManager;
		this.peerServer = peerServer;
		this.address = address;
		this.storage = new SiteStorage(this);
		this.db = new SiteDB(this);
		this.contents = {}; // content.json cache
		this.cntBinding = 0;
		this.bindCalls = 0;
		this.cntContentLoading = 0;
		this.cntContentWriting = 0;
		this.contentWrites = 0;
	}


	async downloadTree(contentPath) {
		// Download content.json. Don't try to download it as a blob as we don't
		// know the hash
		const content = await this.downloadContent(contentPath);

		// Enqueue files for downloading
		if(
			typeof content.files === "object" &&
			content.files !== null &&
			!Array.isArray(content.files)
		) {
			for(const subFilePath of Object.keys(content.files)) {
				const filePath = path.posix.join(path.dirname(contentPath), subFilePath);
				const spec = content.files[subFilePath];
				const id = `${spec.sha512},${spec.size}`;
				this.downloadBlob(filePath, id);
			}
		}

		// Download included content.json's
		if(
			typeof content.includes === "object" &&
			content.includes !== null &&
			!Array.isArray(content.includes)
		) {
			await Promise.all(
				Object.keys(content.includes)
					.map(async subContentPath => {
						const filePath = path.posix.join(path.dirname(contentPath), subContentPath);
						try {
							await this.downloadTree(filePath);
						} catch(e) {
							// TODO
							console.log(e);
						}
					})
			);
		}
	}


	async downloadContent(contentPath) {
		// Check that the path is valid
		if(!this.isValidPath(contentPath)) {
			throw new Error(`Invalid path: ${contentPath}`);
		}

		this.cntContentLoading++;

		await this.workerManager.ensurePeersForSite(this, this.peerServer);

		let content = await this.workerManager.run({
			type: "getContent",
			site: this,
			path: contentPath
		}, 100 - contentPath.length);

		this.cntContentWriting++;
		this.contentWrites++;
		// Save content to filesystem
		const dest = this.resolvePath(contentPath);
		await util.promisify(fs.mkdir)(path.dirname(dest), {
			recursive: true
		});
		await util.promisify(fs.writeFile)(dest, content);
		this.cntContentWriting--;
		this.cntContentLoading--;

		// Import to DB
		await this.db.updateFile(contentPath);

		// Save to memory
		content = JSON.parse(content);
		this.contents[contentPath] = content;
		return content;
	}



	async downloadBlob(filePath, id) {
		// Check that the path is valid
		if(!this.isValidPath(filePath)) {
			throw new Error(`Invalid path: ${filePath}`);
		}
		await this.blobManager.acquire(id);
		await this.blobManager.acquireFile(this.resolvePath(filePath), "download");
		// Download blob
		try {
			await this.blobManager.ensure(id, this, filePath);
			this.cntBinding++;
			this.bindCalls++;
			// Bind it
			try {
				await this.blobManager.bindFrozen(id, this.resolvePath(filePath));
			} finally {
				this.cntBinding--;
			}
		} finally {
			this.blobManager.release(id);
			this.blobManager.releaseFile(this.resolvePath(filePath));
		}
		// Import to DB
		if(filePath === "dbschema.json") {
			// Rebuild the database if required
			this.db.ensureSchema();
		} else if(filePath.endsWith(".json")) {
			// Probably update
			await this.db.updateFile(filePath);
		}
	}


	async downloadBlind() {
		// Don't parallel these tasks! This would collapse them, and
		// downloadBlind would only be called *once*; while we want it to be
		// executed several times (perfectly, by different peers).
		await this.workerManager.ensurePeersForSite(this, this.peerServer);

		for(let i = 0; i < 10; i++) {
			await this.workerManager.run({
				type: "downloadBlind",
				site: this
			}, 20);
		}
	}


	async verifyFile(filePath, content) {
		if(filePath === "content.json" || filePath.endsWith("/content.json")) {
			return await this.verifyContent(filePath, content);
		} else {
			const [parentContent, parentContentPath] = await this.getNearestContent(filePath);
			if(!parentContent) {
				// Oopsie
				return false;
			}
			const innerPath = this.getRelativePath(filePath, parentContentPath);
			const spec = (
				(parentContent.files && parentContent.files[innerPath]) ||
				(parentContent.files_optional && parentContent.files_optional[innerPath])
			);
			if(!spec) {
				// This file shouldn't exist
				return false;
			}
			return (
				spec.size === content.length &&
				spec.sha512 === (
					crypto
						.createHash("sha512")
						.update(content)
						.digest()
						.slice(0, 32)
						.toString("hex")
				)
			);
		}
	}

	async verifyContent(content, contentPath) {
		// Check that the file has valid syntax
		if(!verifyContent(content, contentPath, this.address)) {
			return false;
		}
		content = JSON.parse(content.toString());
		// Check for user content
		const [parentContent, parentContentPath] = await this.getNearestContent(contentPath);
		if(parentContent && parentContent.user_contents) {
			// Aha! Our content.json holds user content -- time to
			// check its signature then
			const innerContentPath = this.getRelativePath(contentPath, parentContentPath);
			if(!verifyUserContentSignatures(content, innerContentPath, parentContent)) {
				return false;
			}
		}
		// Check that this file is not archived
		const fileRules = await this.getFileRules(contentPath, content);
		if(content.modified <= fileRules.archivedBefore) {
			return false;
		}
		// All signatures are correct, only the main signature is
		// left. Check it now!
		const validSigners = fileRules ? fileRules.signers : [];
		return verifyContentSignatures(content, validSigners);
	}


	async getNearestContent(filePath) {
		filePath = filePath.split("/");
		const innerPath = ["content.json"];
		if(filePath.pop() === "content.json") {
			if(filePath.length === 0) {
				// getNearestContent("content.json") was called
				return [null, null];
			}
			innerPath.unshift(filePath.pop());
		}
		while(filePath.length > 0) {
			const contentPath = [].concat(filePath, ["content.json"]).join("/");
			let content;
			try {
				content = await this.readContent(contentPath);
			} catch(e) {
				// This file doesn't exist or is invalid, move on
				innerPath.unshift(filePath.pop());
				continue;
			}
			if(content && content.includes && content.includes[innerPath.join("/")]) {
				// Our file is included
				break;
			} else if(content && content.user_contents) {
				// This file handles user content
				break;
			}
			// Move on
			innerPath.unshift(filePath.pop());
		}
		filePath.push("content.json");
		filePath = filePath.join("/");
		return [await this.readContent(filePath), filePath];
	}

	async getFileRules(filePath, origContent=null) {
		const [content, contentPath] = await this.getNearestContent(filePath);
		if(!content) {
			// Root content.json
			return {
				signers: [this.address],
				archivedBefore: 0
			};
		}
		const innerPath = this.getRelativePath(filePath, contentPath);
		if(content.includes && content.includes[innerPath]) {
			// Our file is included
			return {
				signers: [this.address].concat(content.includes[innerPath].signers),
				archivedBefore: 0
			};
		} else if(content.user_contents) {
			// This file handles user content
			if(!origContent && filePath.endsWith("/content.json")) {
				// Try to use an older version
				try {
					origContent = await this.readContent(filePath);
				// eslint-disable-next-line no-empty
				} catch(e) {
				}
			}
			return await this.getUserContentRules(innerPath, content.user_contents, origContent);
		} else {
			return null;
		}
	}

	getUserContentRules(relativePath, userContents, origContent=null) {
		const authAddress = relativePath.split("/")[0];
		const certUserId = (origContent && origContent.cert_user_id) || "n-a@n-a";
		const exCertUserId = `${(origContent && origContent.cert_auth_type) || "n-a"}#${certUserId}`;

		let rules = {
			signers: []
		};
		const applyRule = rule => {
			if(rules === null) {
				return;
			}
			if(!rule) {
				// Banned
				rules = null;
				return;
			}

			if(typeof rule === "object" && !Array.isArray(rule)) {
				for(const key of Object.keys(rule)) {
					if(typeof rules[key] === "undefined") {
						// Just update
						rules[key] = rule[key];
					} else if(typeof rules[key] === typeof rule[key]) {
						if(typeof rule[key] === "boolean") {
							rules[key] = rules[key] || rule[key];
						} else if(typeof rule[key] === "number") {
							rules[key] = Math.max(rules[key], rule[key]);
						} else if(typeof rule[key] === "string") {
							// I don't understand this part TBH, it doesn't make
							// sense to compare strings by length here
							if(rule[key].length > rules[key].length) {
								rules[key] = rule[key];
							}
						} else {
							// What?
							Config.logger.warn(`${this}: Weird user content rule type for ${relativePath}: ${typeof rule[key]}`);
						}
					} else {
						// Shouldn't happen TBH
						Config.logger.warn(`${this}: Weird rule override for ${relativePath}: was ${typeof rules[key]}, new value is ${typeof rule[key]}`);
					}
				}
			}
		};

		if(userContents.permission_rules) {
			for(const regex of Object.keys(userContents.permission_rules)) {
				if(
					(new RegExp(regex)).test(authAddress) ||
					(new RegExp(regex)).test(exCertUserId)
				) {
					// Apply this rule
					applyRule(userContents.permission_rules[regex]);
				}
			}
		}
		if(userContents.permissions) {
			for(const key of Object.keys(userContents.permissions)) {
				if(key === certUserId) {
					// Apply this rule
					applyRule(userContents.permissions[key]);
				}
			}
		}

		if(rules) {
			// Not banned

			// Archive date
			rules.archivedBefore = 0;
			if(userContents.archived && userContents.archived[authAddress]) {
				rules.archivedBefore = Math.max(
					rules.archivedBefore,
					userContents.archived[authAddress]
				);
			}
			if(userContents.archived_before) {
				rules.archivedBefore = Math.max(
					rules.archivedBefore,
					userContents.archived_before
				);
			}

			rules.signers.push(authAddress);
			// Site address is always valid
			rules.signers.push(this.address);
		}

		return rules;
	}


	async readContent(contentPath) {
		if(this.contents[contentPath] === null) {
			// This content.json was checked; it doesn't exist
			throw new Error(`${contentPath} doesn't exist`);
		} else if(this.contents[contentPath]) {
			// This content.json is cached
			return this.contents[contentPath];
		} else {
			try {
				const content = JSON.parse(
					await this.readFile(contentPath)
				);
				this.contents[contentPath] = content;
				return content;
			} catch(e) {
				// Oopsie
				// Don't try to load this content.json again
				this.contents[contentPath] = null;
				throw e;
			}
		}
	}


	// Download a file if required
	async needFile(filePath) {
		// Was that file downloaded already?
		try {
			const stat = await util.promisify(fs.stat)(this.resolvePath(filePath));
			return stat.isDirectory() ? "directory" : true;
		// eslint-disable-next-line no-empty
		} catch(e) {
		}

		// We walk the content.json tree until we find the content.json that
		// includes the file we need
		let currentContentPath = "content.json";
		const contentPathsHistory = [];
		// eslint-disable-next-line no-constant-condition
		while(true) {
			contentPathsHistory.push(currentContentPath);

			let currentContent;
			try {
				currentContent = await this.readContent(currentContentPath);
			} catch(e) {
				currentContent = await this.downloadContent(currentContentPath);
				this.downloadTree(currentContentPath); // in background
			}

			const innerPath = filePath
				.split("/")
				.slice(currentContentPath.split("/").length - 1)
				.join("/");

			if(innerPath === "") {
				// It's, like, this directory itself... no problem
				return "directory";
			}

			if(currentContentPath === filePath) {
				// Hooray!
				return true;
			}

			if(currentContent.files && currentContent.files[innerPath]) {
				// Yay!
				const spec = currentContent.files[innerPath];
				const id = `${spec.sha512},${spec.size}`;
				await this.downloadBlob(filePath, id);
				return true;
			}
			if(currentContent.files_optional && currentContent.files_optional[innerPath]) {
				// Copy-pasta you said?..
				const spec = currentContent.files_optional[innerPath];
				const id = `${spec.sha512},${spec.size}`;
				await this.downloadBlob(filePath, id);
				return true;
			}
			if(
				(
					currentContent.files &&
					Object.keys(currentContent.files).some(f => f.startsWith(`${innerPath}/`))
				) || (
					currentContent.files_optional &&
					Object.keys(currentContent.files_optional).some(f => f.startsWith(`${innerPath}/`))
				)
			) {
				// What we're searching for is actually a directory
				return "directory";
			}

			// The logic below is similar to getFileRules() logic (the same keys
			// are checked)
			if(currentContent.includes) {
				const matchingIncludes = (
					Object.keys(currentContent.includes).filter(key => (
						filePath.startsWith(
							key.replace(/content\.json$/, "")
						)
					))
				);
				if(matchingIncludes.length === 1) {
					// :tada:
					currentContentPath = matchingIncludes[0];
					continue;
				} else if(matchingIncludes.length > 1) {
					// There's a problem with content.json. Usually this
					// shouldn't happen, and I haven't found any sites that have
					// this problem as well. However, it makes sense to check
					// the largest matching prefix.
					// This implementation is not optimal, however, we shouldn't
					// have reached this code in the first place, so it's
					// doesn't really matter.
					currentContentPath = matchingIncludes
						.sort((a, b) => b.length - a.length)[0];
					continue;
				}
			}

			if(currentContent.user_contents) {
				// filePath = data/users/1Cy3.../data.json
				// currentContentPath = data/users/content.json
				// ->
				// currentContentPath = data/users/1Cy3.../content.json
				currentContentPath = (
					filePath
						.split("/")
						.slice(0, currentContentPath.split("/").length + 1)
						.concat(["content.json"])
						.join("/")
				);
				continue;
			}

			// ...uhm?
			throw new Error(
				// eslint-disable-next-line prefer-template
				`Could not find ancestor for ${filePath}. Content stack: ` +
				contentPathsHistory.join(" -> ")
			);
		}
	}


	async readFile(filePath) {
		const absPath = this.resolvePath(filePath);
		await this.blobManager.acquireFile(absPath, "read");
		try {
			return await util.promisify(fs.readFile)(absPath);
		} finally {
			this.blobManager.releaseFile(absPath);
		}
	}

	async walk(dirPath="") {
		const list = [];

		const _walk = async (fsRoot, relativeRoot) => {
			const stat = await util.promisify(fs.stat)(fsRoot);
			if(stat.isDirectory()) {
				const children = await util.promisify(fs.readdir)(fsRoot);
				await Promise.all(children.map(c => (
					_walk(
						path.join(fsRoot, c),
						path.posix.join(relativeRoot, c)
					)
				)));
			} else {
				list.push(relativeRoot);
			}
		};

		await _walk(this.resolvePath(dirPath), dirPath);
		return list;
	}


	getRelativePath(filePath, relativeTo) {
		return filePath.split("/").slice(relativeTo.split("/").length - 1).join("/");
	}
	isValidPath(filePath) {
		return (
			filePath.indexOf("\\") === -1 &&
			filePath !== ".." &&
			!filePath.startsWith("../") &&
			!filePath.endsWith("/..") &&
			filePath.indexOf("/../") === -1
		);
	}
	resolvePath(filePath) {
		return path.join(
			Config.dataDirectory,
			"sites",
			this.address,
			filePath
		);
	}


	toString() {
		return `[Site ${this.address}]`;
	}
}