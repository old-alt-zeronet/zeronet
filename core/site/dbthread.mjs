import util from "util";
import fs from "fs";
import path from "path";
import sqlite from "sqlite-async";
import RWLock from "async-rwlock";
import * as SafeRe from "../safere";
import _Mutex from "await-mutex";
const Mutex = _Mutex.default; // Someone doesn't know how to use ESM modules


const siteRoot = process.argv[2];
const openMutex = new Mutex();
const ensureLock = new RWLock.RWLock();
let db = null;
let dbPath = null;
let schema = null;
const currentTasks = [];


async function open() {
	if(db) {
		return;
	}
	if(openMutex.isLocked()) {
		// Wait till another thread opens the database
		const unlock = await openMutex.lock();
		unlock();
		return;
	}
	const unlock = await openMutex.lock();
	// We're using a mutex here, so the updates are atomic already
	/* eslint-disable require-atomic-updates */
	if(!db) {
		try {
			await util.promisify(fs.mkdir)(path.dirname(dbPath), {
				recursive: true
			});
		// eslint-disable-next-line no-empty
		} catch(e) {
		}
		db = await sqlite.open(dbPath);
	}
	/* eslint-enable require-atomic-updates */
	unlock();
}


async function ensureSchema() {
	await ensureLock.writeLock();

	await loadVerifySchema();
	dbPath = path.join(siteRoot, schema.db_file);

	try {
		await open();

		// Load current schema
		let versions;
		try {
			versions = await db.all(
				`SELECT * FROM keyvalue WHERE key LIKE "table.%.version"`
			);
		} catch(e) {
			versions = [];
			// Create an empty keyvalue table. We should have added
			// "REFERENCES json (json_id)" constraint here, however, we
			// don't have the json table at the moment
			await db.exec(`
				CREATE TABLE keyvalue (
					keyvalue_id INTEGER PRIMARY KEY AUTOINCREMENT,
					key TEXT,
					value BLOB,
					json_id INTEGER
				)
			`);
		}

		// List modifications
		const modificationsRequired = [];
		let createImplictJson = false;
		for(const v of versions) {
			const table = v.key.match(/^table\.(.*)\.version$/)[1];
			const version = v.value;
			if(!schema.tables[table]) {
				if(table === "json") {
					if(version !== null) {
						modificationsRequired.push({
							type: "delete",
							table
						});
						createImplictJson = true;
					}
				} else {
					modificationsRequired.push({
						type: "delete",
						table
					});
				}
			} else if(schema.tables[table].schema_changed !== version) {
				modificationsRequired.push({
					type: "delete",
					table
				});
				modificationsRequired.push({
					type: "create",
					table
				});
			}
		}
		for(const table of Object.keys(schema.tables)) {
			if(!versions.some(v => v.key === `table.${table}.version`)) {
				modificationsRequired.push({
					type: "create",
					table
				});
			}
		}
		if(
			!versions.some(v => v.key === "table.json.version") &&
			!schema.tables.json
		) {
			createImplictJson = true;
		}

		if(modificationsRequired.length === 0 && !createImplictJson) {
			// Hooray!
			return;
		}

		// Oof, rebuild is required
		console.log("Rebuilding DB...");
		if(createImplictJson) {
			const jsonSchema = {
				1: "path TEXT",
				2: "directory TEXT, file_name TEXT",
				3: "site TEXT, directory TEXT, file_name TEXT"
			}[schema.version];

			await db.exec(`
				CREATE TABLE json (
					json_id INTEGER PRIMARY KEY AUTOINCREMENT,
					${jsonSchema}
				);

				INSERT INTO keyvalue (key, value, json_id) VALUES (
					"table.json.version",
					NULL,
					0
				)
			`);
		}
		for(const {type, table} of modificationsRequired) {
			if(type === "delete") {
				await db.exec(`
					DELETE TABLE ${table};

					DELETE FROM keyvalue WHERE (
						key = "table.${table}.version"
					)
				`);
			} else if(type === "create") {
				await db.exec(`
					CREATE TABLE ${table}
					(${
						schema.tables[table].cols
							.map(([name, spec]) => `${name} ${spec}`)
							.join(", ")
					});

					INSERT INTO keyvalue (key, value, json_id) VALUES (
						"table.${table}.version",
						${schema.tables[table].schema_changed},
						0
					)
				`);
			}
		}
	} finally {
		ensureLock.unlock();
	}

	console.log("Importing files...");

	const walk = async (fsRoot, relativeRoot) => {
		const stat = await util.promisify(fs.stat)(fsRoot);
		if(stat.isDirectory()) {
			const children = await util.promisify(fs.readdir)(fsRoot);
			await Promise.all(children.map(c => (
				walk(
					path.join(fsRoot, c),
					path.posix.join(relativeRoot, c)
				)
			)));
		} else {
			if(relativeRoot.endsWith(".json")) {
				// Potentionally handles DB content
				await updateFile(relativeRoot, Date.now());
			}
		}
	};
	await walk(siteRoot, "");

	console.log("Done");
}


async function loadVerifySchema() {
	const loadedSchema = JSON.parse(
		await util.promisify(fs.readFile)(
			path.join(siteRoot, "dbschema.json")
		)
	);
	if([1, 2, 3].indexOf(loadedSchema.version) === -1) {
		throw new Error("version must be 1, 2 or 3");
	}
	if(
		typeof loadedSchema.tables !== "object" ||
		Array.isArray(loadedSchema.tables)
	) {
		throw new Error("'tables' must be a map");
	}
	if(
		Object.keys(loadedSchema.tables).some(
			table => !/^[a-z_][a-z0-9_]*$/i.test(table)
		)
	) {
		throw new Error("table name is invalid");
	}
	if(
		Object.keys(loadedSchema.tables).some(
			table => !Array.isArray(loadedSchema.tables[table].cols)
		)
	) {
		throw new Error("table must contain columns");
	}
	if(
		Object.keys(loadedSchema.tables).some(
			table => loadedSchema.tables[table].cols.some(
				col => !/^[a-z_][a-z0-9_]*$/i.test(col[0])
			)
		)
	) {
		throw new Error("column name is invalid");
	}
	// Human-readable regex below:
	// /
	//     ^
	//     (
	//         int|integer|tinyint|smallint|mediumint|bigint|
	//         unsigned big int|int2|int8|
	//
	//         (character|varchar|varying character|nvarchar)\(\d+\)|
	//         text|clob|
	//
	//         blob|
	//
	//         real|double|double precision|float|
	//
	//         numeric|decimal\(\d+, ?\d+\)|boolean|date|datetime
	//     )
	//     (
	//         ( constraint [a-z_][a-z0-9_]*)?
	//         [ ]
	//         (
	//             primary key
	//             ( asc| desc)?
	//             ( on conflict (rollback|abort|fail|ignore|replace))?
	//             ( autoincrement)?
	//         |
	//             not null
	//             ( on conflict (rollback|abort|fail|ignore|replace))?
	//         |
	//             unique
	//             ( on conflict (rollback|abort|fail|ignore|replace))?
	//         |
	//             default (
	//                 [+-]?(\d+|\.\d*|\d+\.\d*)|
	//                 '([^']|'')*'|
	//                 "([^"]|"")*"|
	//                 null|true|false|
	//                 current_time|current_date|current_timestamp
	//             )
	//         |
	//             references [a-z_][a-z0-9_]* ?
	//             \(
	//                 [a-z_][a-z0-9_]*(, ?[a-z_][a-z0-9_]*)*
	//             \)
	//         )*
	//     )*
	//     $
	// /i
	if(
		Object.keys(loadedSchema.tables).some(
			table => loadedSchema.tables[table].cols.some(
				col => !/^(int|integer|tinyint|smallint|mediumint|bigint|unsigned big int|int2|int8|(character|varchar|varying character|nvarchar)\(\d+\)|text|clob|blob|real|double|double precision|float|numeric|decimal\(\d+, ?\d+\)|boolean|date|datetime)(( constraint [a-z_][a-z0-9_]*)?[ ](primary key( asc| desc)?( on conflict (rollback|abort|fail|ignore|replace))?( autoincrement)?|not null( on conflict (rollback|abort|fail|ignore|replace))?|unique( on conflict (rollback|abort|fail|ignore|replace))?|default ([+-]?(\d+|\.\d*|\d+\.\d*)|'([^']|'')*'|"([^"]|"")*"|null|true|false|current_time|current_date|current_timestamp)|references [a-z_][a-z0-9_]* ?\([a-z_][a-z0-9_]*(, ?[a-z_][a-z0-9_]*)*\))*)*$/i
					.test(col[1].trim().replace(/\s+/g, " "))
			)
		)
	) {
		throw new Error("column type is invalid");
	}
	if(
		Object.keys(loadedSchema.tables).some(
			table => (
				typeof loadedSchema.tables[table].schema_changed !== "number"
			)
		)
	) {
		throw new Error(
			"schema_changed must be a number"
		);
	}
	if(typeof loadedSchema.db_file !== "string") {
		throw new Error("db_file must be a string");
	}
	if(
		typeof loadedSchema.maps !== "object" ||
		Array.isArray(loadedSchema.maps)
	) {
		throw new Error("'maps' must be a map");
	}
	if(
		Object.keys(loadedSchema.maps).some(
			pattern => (
				typeof loadedSchema.maps[pattern] !== "object" ||
				Array.isArray(loadedSchema.maps[pattern])
			)
		)
	) {
		throw new Error("mapping must be a map");
	}
	if(
		Object.keys(loadedSchema.maps).some(
			pattern => (
				loadedSchema.maps[pattern].to_table &&
				!Array.isArray(loadedSchema.maps[pattern].to_table)
			)
		)
	) {
		throw new Error("to_table must be an array");
	}
	if(
		Object.keys(loadedSchema.maps).some(
			pattern => (
				loadedSchema.maps[pattern].to_keyvalue &&
				(
					!Array.isArray(loadedSchema.maps[pattern].to_keyvalue) ||
					loadedSchema.maps[pattern].to_keyvalue.some(
						s => typeof s !== "string"
					)
				)
			)
		)
	) {
		throw new Error(
			"to_keyvalue must be an array of strings"
		);
	}
	if(
		Object.keys(loadedSchema.maps).some(
			pattern => (
				loadedSchema.maps[pattern].to_json_table &&
				(
					!Array.isArray(
						loadedSchema.maps[pattern].to_json_table
					) ||
					loadedSchema.maps[pattern].to_json_table.some(
						s => typeof s !== "string"
					)
				)
			)
		)
	) {
		throw new Error(
			"to_json_table must be an array of strings"
		);
	}
	if(
		Object.keys(loadedSchema.maps).some(
			pattern => (
				loadedSchema.maps[pattern].to_table &&
				!Array.isArray(loadedSchema.maps[pattern].to_table)
			)
		)
	) {
		throw new Error(
			"to_table must be an array"
		);
	}
	if(
		Object.keys(loadedSchema.maps).some(
			pattern => (
				loadedSchema.maps[pattern].to_table &&
				loadedSchema.maps[pattern].to_table.some(
					t => (
						typeof t !== "string" &&
						(
							typeof t !== "object" ||
							Array.isArray(t)
						)
					)
				)
			)
		)
	) {
		throw new Error(
			"to_table values must be strings or mappings"
		);
	}
	if(
		Object.keys(loadedSchema.maps).some(
			pattern => (
				loadedSchema.maps[pattern].to_table &&
				loadedSchema.maps[pattern].to_table.some(
					t => (
						typeof t === "object" &&
						typeof t.table !== "string"
					)
				)
			)
		)
	) {
		throw new Error(
			"to_table values must contain a string 'table' key"
		);
	}
	if(
		Object.keys(loadedSchema.maps).some(
			pattern => (
				loadedSchema.maps[pattern].to_table &&
				loadedSchema.maps[pattern].to_table.some(
					t => (
						typeof t === "object" &&
						Object.prototype.hasOwnProperty.call(t, "node") &&
						typeof t.node !== "string"
					)
				)
			)
		)
	) {
		throw new Error(
			"to_table.*.node may only be a string"
		);
	}
	if(
		Object.keys(loadedSchema.maps).some(
			pattern => (
				loadedSchema.maps[pattern].to_table &&
				loadedSchema.maps[pattern].to_table.some(
					t => (
						typeof t === "object" &&
						(
							(
								Object.prototype.hasOwnProperty.call(t, "key_col") &&
								typeof t.key_col !== "string"
							) ||
							(
								Object.prototype.hasOwnProperty.call(t, "val_col") &&
								typeof t.val_col !== "string"
							)
						)
					)
				)
			)
		)
	) {
		throw new Error(
			"to_table.*.key_col/val_col may only be a string"
		);
	}
	if(
		Object.keys(loadedSchema.maps).some(
			pattern => (
				loadedSchema.maps[pattern].to_table &&
				loadedSchema.maps[pattern].to_table.some(
					t => (
						typeof t === "object" &&
						Object.prototype.hasOwnProperty.call(t, "import_cols") &&
						(
							!Array.isArray(t.import_cols) ||
							t.import_cols.some(c => typeof c !== "string")
						)
					)
				)
			)
		)
	) {
		throw new Error(
			"to_table.*.import_cols may only be an array of strings"
		);
	}
	if(
		Object.keys(loadedSchema.maps).some(
			pattern => (
				loadedSchema.maps[pattern].to_table &&
				loadedSchema.maps[pattern].to_table.some(
					t => (
						typeof t === "object" &&
						Object.prototype.hasOwnProperty.call(t, "replaces") &&
						(
							typeof t.replaces !== "object" ||
							Array.isArray(t.replaces) ||
							Object.keys(t.replaces).some(
								c => (
									typeof t.replaces[c] !== "object" ||
									Array.isArray(t.replaces[c]) ||
									t.replaces[c].some(v => typeof v !== "string")
								)
							)
						)
					)
				)
			)
		)
	) {
		throw new Error(
			"to_table.*.replaces may only be a mapping of mappings of strings"
		);
	}

	schema = loadedSchema;
}


async function updateFile(filePath, sentTime) {
	if(!schema) {
		return;
	}

	const dbDir = path.dirname(schema.db_file);
	if(!filePath.startsWith(`${dbDir}/`)) {
		// Not inside data directory
		return;
	}

	const task = {
		filePath,
		status: "opening",
		timeToStart: Date.now() - sentTime
	};
	currentTasks.push(task);

	await open();

	task.status = "reading";

	let content;
	try {
		content = JSON.parse(
			await util.promisify(fs.readFile)(
				path.join(siteRoot, filePath)
			)
		);
	} catch(e) {
		// It's okay to store non-JSON-encoded data in .json
		currentTasks.splice(currentTasks.indexOf(task), 1);
		return;
	}
	if(typeof content !== "object" || Array.isArray(content)) {
		// Same
		currentTasks.splice(currentTasks.indexOf(task), 1);
		return;
	}

	const relativePath = filePath
		.split("/")
		.slice(schema.db_file.split("/").length - 1)
		.join("/");

	task.status = "locking";

	await ensureLock.readLock();

	task.status = "cleanjson";

	// Remove from json table
	if(schema.version === 1) {
		await run(
			"DELETE FROM json WHERE path = :path",
			{
				path: relativePath
			}
		);
	} else if(schema.version === 2) {
		await run(
			`
				DELETE FROM json
				WHERE directory = :directory AND file_name = :fileName
			`,
			{
				directory: relativePath.split("/").slice(0, -1).join("/"),
				fileName: relativePath.split("/").slice(-1)[0]
			}
		);
	} else if(schema.version === 3) {
		await run(
			`
				DELETE FROM json
				WHERE
					site = :site AND
					directory = :directory AND
					file_name = :fileName
			`,
			{
				site: relativePath.split("/")[0],
				directory: relativePath.split("/").slice(1, -1).join("/"),
				fileName: relativePath.split("/").slice(-1)[0]
			}
		);
	}

	task.status = "prep json";

	let jsonKeys = "";
	let jsonVariables = "";
	const jsonValues = {};
	for(const pattern of Object.keys(schema.maps)) {
		if(await SafeRe.test(pattern, relativePath)) {
			if(schema.maps[pattern].to_json_table) {
				for(const key of schema.maps[pattern].to_json_table) {
					jsonKeys += `, ${key}`;
					jsonVariables += `, :${key}`;
					jsonValues[key] = Object.prototype.hasOwnProperty.call(content, key)
						? content[key]
						: null;
				}
			}
		}
	}

	task.status = "ins  json";

	let jsonId;
	if(schema.version === 1) {
		jsonId = (await run(
			`
				INSERT INTO json (path${jsonKeys})
				VALUES (:path${jsonVariables})
			`,
			{
				path: relativePath,
				...jsonValues
			}
		)).lastID;
	} else if(schema.version === 2) {
		jsonId = (await run(
			`
				INSERT INTO json (directory, file_name${jsonKeys})
				VALUES (:directory, :fileName${jsonVariables})
			`,
			{
				directory: relativePath.split("/").slice(0, -1).join("/"),
				fileName: relativePath.split("/").slice(-1)[0],
				...jsonValues
			}
		)).lastID;
	} else if(schema.version === 3) {
		jsonId = (await run(
			`
				INSERT INTO json (site, directory, file_name${jsonKeys})
				VALUES (:site, :directory, :fileName${jsonVariables})
			`,
			{
				site: relativePath.split("/")[0],
				directory: relativePath.split("/").slice(1, -1).join("/"),
				fileName: relativePath.split("/").slice(-1)[0],
				...jsonValues
			}
		)).lastID;
	}

	task.status = "insert";

	// Finally insert the file contents
	for(const pattern of Object.keys(schema.maps)) {
		if(await SafeRe.test(pattern, relativePath)) {
			await _insertData(
				content,
				schema.maps[pattern],
				jsonId
			);
		}
	}

	ensureLock.unlock();

	currentTasks.splice(currentTasks.indexOf(task), 1);
}

async function _insertData(content, map, jsonId) {
	if(map.to_keyvalue) {
		await _insertKeyValueData(content, map.to_keyvalue, jsonId);
	}

	if(map.to_table) {
		for(const tblConfig of map.to_table) {
			await _insertTableData(content, tblConfig, jsonId);
		}
	}
}

async function _insertKeyValueData(content, config, jsonId) {
	// Cleanup old rows
	await run(
		"DELETE FROM keyvalue WHERE json_id = :jsonId",
		{jsonId}
	);
	// Add new ones
	for(const key of config) {
		if(Object.prototype.hasOwnProperty.call(content, key)) {
			await run(
				`
					INSERT INTO keyvalue (key, value, json_id)
					VALUES (:key, :value, :jsonId)
				`,
				{
					key,
					value: content[key],
					jsonId
				}
			);
		}
	}
}

async function _insertTableData(content, config, jsonId) {
	let table;
	let node;
	let keyCol;
	let valCol;
	let importCols;
	let replaces;
	if(typeof config === "string") {
		table = config;
		node = config;
		keyCol = null;
		valCol = null;
		importCols = null;
		replaces = {};
	} else {
		table = config.table;
		node = config.node || table;
		keyCol = config.key_col || null;
		valCol = config.val_col || null;
		importCols = config.import_cols || null;
		replaces = config.replaces || {};
	}

	if(!schema.tables[table]) {
		// You're sure?
		return;
	}

	if(importCols === null) {
		importCols = schema.tables[table].cols.map(col => col[0]);
	}

	// Cleanup old rows
	await run(
		`DELETE FROM ${table} WHERE json_id = :jsonId`,
		{jsonId}
	);
	// Add new ones
	if(!keyCol && Array.isArray(content[node])) {
		// Plain rows
		for(const row of content[node]) {
			if(typeof row === "object" && !Array.isArray(row)) {
				const replacedRow = _doReplace(row, replaces);
				const keys = Object.keys(replacedRow).filter(
					key => importCols.indexOf(key) > -1
				);
				const variables = keys.map(x => `:${x}`);
				const values = {};
				for(const key of Object.keys(replacedRow)) {
					if(importCols.indexOf(key) > -1) {
						values[key] = replacedRow[key];
					}
				}
				await run(
					`
						INSERT INTO ${table} (${keys.join(", ")}, json_id)
						VALUES (${variables.join(", ")}, :json_id)
					`,
					{
						...values,
						json_id: jsonId // eslint-disable-line camelcase
					}
				);
			}
		}
	} else if(
		keyCol && !valCol &&
		typeof content[node] === "object" &&
		!Array.isArray(content[node])
	) {
		// Primary key
		for(const primaryKey of Object.keys(content[node])) {
			let rows = content[node][primaryKey];
			if(!Array.isArray(rows)) {
				rows = [rows];
			}
			for(const row of rows) {
				const replacedRow = _doReplace(row, replaces);
				const keys = Object.keys(replacedRow).filter(
					key => importCols.indexOf(key) > -1
				);
				const variables = keys.map(x => `:${x}`);
				const values = {};
				for(const key of Object.keys(replacedRow)) {
					if(importCols.indexOf(key) > -1) {
						values[key] = replacedRow[key];
					}
				}
				await run(
					`
						INSERT INTO ${table} (${keys.join(", ")}, ${keyCol}, json_id)
						VALUES (${variables.join(", ")}, :${keyCol}, :json_id)
					`,
					{
						...values,
						[keyCol]: primaryKey,
						json_id: jsonId // eslint-disable-line camelcase
					}
				);
			}
		}
	} else if(
		keyCol && valCol && typeof content[node] === "object"
	) {
		// Key-value
		for(const key of Object.keys(content[node])) {
			const value = content[node][key];
			const {
				key: replacedKey,
				value: replacedValue
			} = _doReplace({key, value}, replaces);
			await run(
				`
					INSERT INTO ${table} (${keyCol}, ${valCol}, json_id)
					VALUES (:key, :value, :jsonId)
				`,
				{
					key: replacedKey,
					value: replacedValue,
					jsonId
				}
			);
		}
	}
}

function _doReplace(row, replaces) {
	if(!replaces) {
		return row;
	}
	const replacedRow = {...row};
	for(const key of Object.keys(replaces)) {
		if(typeof replacedRow[key] === "string") {
			for(const from of Object.keys(replaces[key])) {
				replacedRow[key] = replacedRow[key]
					.split(from)
					.join(replaces[key][from]);
			}
		}
	}
	return replacedRow;
}


async function _with(cmd, query, params) {
	if(typeof params === "object" && !Array.isArray(params)) {
		const newParams = {};
		for(const key of Object.keys(params)) {
			newParams[`:${key}`] = params[key];
		}
		params = newParams;
	}

	const stmt = await db.prepare(query);
	const res = await stmt[cmd](params);
	await stmt.finalize();
	return res;
}
async function run(query, params) {
	return await _with("run", query, params);
}



// Setup IPC
process.on("message", async message => {
	if(message.cmd === "ensureSchema") {
		try {
			await ensureSchema();
			process.send({
				response: null,
				to: message.id
			});
		} catch(e) {
			process.send({
				response: e.toString(),
				to: message.id
			});
		}
	} else if(message.cmd === "updateFile") {
		await updateFile(message.args.filePath, message.args.sentTime);
		process.send({
			response: null,
			to: message.id
		});
	} else if(message.cmd === "getInfo") {
		process.send({
			response: {
				isOpen: !!db,
				dbPath,
				schema,
				openMutex: openMutex.isLocked(),
				ensureLock: ensureLock.getState(),
				currentTasks
			},
			to: message.id
		});
	}
});