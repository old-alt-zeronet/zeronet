import Config from "../../config";
import Site from ".";


export default class SiteManager {
	constructor(workerManager, blobManager, peerServer) {
		this.workerManager = workerManager;
		this.blobManager = blobManager;
		this.peerServer = peerServer;

		this.sites = {};
		// Load sites from config
		for(const address of Object.keys(Config.sites)) {
			this._addSite(address);
			// Update site
			this.sites[address].downloadBlind();
		}
	}


	get(address) {
		if(!this.sites[address]) {
			this._addSite(address);
			Config.sites[address] = {};
			Config.saveSites();
		}
		return this.sites[address];
	}

	_addSite(address) {
		this.sites[address] = new Site(
			address,
			this.workerManager,
			this.blobManager,
			this.peerServer
		);
	}
}