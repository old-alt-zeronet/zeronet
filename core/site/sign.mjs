import {verify, recover} from "../crypt";

// The code below looks *much* better without templates
/* eslint-disable prefer-template */
function stableStringify(obj) {
	if(Array.isArray(obj)) {
		return `[${obj.map(x => stableStringify(x)).join(", ")}]`;
	} else if(typeof obj === "object" && obj !== null) {
		return (
			"{" +
			(
				Object.keys(obj)
					.sort()
					.map(key => `${stableStringify(key)}: ${stableStringify(obj[key])}`)
					.join(", ")
			) +
			"}"
		);
	} else if(typeof obj === "string") {
		// We can't just use JSON.stringify() here because the official
		// implementation (i.e. Python) adds \u1234 sometimes when Node adds raw
		// character. For example, JSON.stringify("д") (Russian character) gives
		// "\"д\"", while json.dumps("д") gives "\\u0434". The code below is
		// ported from Python's json.py_encode_basestring_ascii implementation.
		return (
			"\"" +
			obj.replace(
				/[\\"]|[^\x20-\x7e]/g,
				c => {
					const n = c.charCodeAt(0);
					return {
						"\\": String.raw`\\`,
						"\"": String.raw`\"`,
						"\b": String.raw`\b`,
						"\f": String.raw`\f`,
						"\n": String.raw`\n`,
						"\r": String.raw`\r`,
						"\t": String.raw`\t`
					}[c] || (
						n < 0x10000
							? String.raw`\u${n.toString(16).padStart(4, "0")}`
							: (
								"\\u" + (
									(0xd800 | (((n - 0x10000) >> 10) & 0x3ff))
										.toString(16)
										.padStart(4, "0")
								) +
								"\\u" + (
									(0xdc00 | ((n - 0x10000) & 0x3ff))
										.toString(16)
										.padStart(4, "0")
								)
							)
					);
				}
			) +
			"\""
		);
	} else {
		return JSON.stringify(obj);
	}
}
/* eslint-enable prefer-template */

export function verifyContent(content, contentPath, siteAddress) {
	try {
		content = JSON.parse(content.toString());
	} catch(e) {
		// Invalid JSON
		return false;
	}

	if(content.address && content.address !== siteAddress) {
		// Wrong address
		// In latest ZeroNet versions (0.7.0 for sure), content.address is
		// *always* set. However, I've noticed that some user content.json's
		// don't have this property (more specifically, the oldest file I could
		// fine was dated 2016-02-13). These files *used* to be correct, so we
		// still support them.
		return false;
	}

	if(content.inner_path && content.inner_path !== contentPath) {
		// Wrong file path
		// Same story here
		return false;
	}

	return true;
}

export function verifyUserContentSignatures(content, innerContentPath, parentContent) {
	if(
		typeof content.cert_sign !== "string" ||
		typeof content.cert_user_id !== "string" ||
		typeof content.cert_auth_type !== "string" ||
		content.cert_user_id.split("@").length !== 2
	) {
		// Oopsie? Probably not a user content.json
		return true;
	}

	const [username, provider] = content.cert_user_id.split("@", 2);
	const authAddress = innerContentPath.split("/")[0];

	const data = `${authAddress}#${content.cert_auth_type}/${username}`;

	let recoveredAddress;
	try {
		recoveredAddress = recover(data, content.cert_sign);
	} catch(e) {
		return false;
	}

	const certSigners = parentContent.user_contents.cert_signers;
	return (
		certSigners &&
		typeof certSigners === "object" &&
		Array.isArray(certSigners[provider]) &&
		certSigners[provider].indexOf(recoveredAddress) > -1
	);
}

export function verifyContentSignatures(content, validSigners) {
	// Check signatures
	let newContent = {...content};
	delete newContent.sign; // just in case
	delete newContent.signs;
	newContent = stableStringify(newContent);
	for(const signer of validSigners) {
		if(content.signs && content.signs[signer]) {
			if(verify(newContent, content.signs[signer], signer)) {
				// Yay!
				return true;
			}
		}
	}

	// Oopsie
	return false;
}