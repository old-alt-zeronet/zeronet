import net from "net";
import Config from "../../../config";
import Transport from "../transport";
import PeerClient from "../client";


export default class PeerServer {
	constructor(port, workerManager, discovery) {
		this.peerId = "-UT3530-";
		for(let i = 0; i < 12; i++) {
			const alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			this.peerId += alphabet[Math.floor(Math.random() * alphabet.length)];
		}

		this.port = port;
		this.workerManager = workerManager;
		this.discovery = discovery;

		this.discovery.on("newPeer", (addr, peerData) => {
			this.connect(addr, peerData);
		});

		this.peers = {};
		this.workers = {};

		this.server = net.createServer(this.handleRequest.bind(this));
		this.server.on("listening", () => {
			Config.logger.info(`PeerServer: Server listening on :${port}`);
		});
	}
	serve() {
		this.server.listen(this.port, "0.0.0.0"); // TODO: don't force IPv4
	}

	connect(addr, peerData) {
		if(this.peers[addr]) {
			// Done already, but we have to update peerData
			this.workers[addr].updatePeerData(peerData);
			return;
		}
		// A new peer
		const peer = new PeerClient(this);
		peer.connect(addr);
		const worker = this.workerManager.addPeer(peer);
		worker.updatePeerData(peerData);
		this.peers[addr] = peer;
		this.workers[addr] = worker;
		peer.once("disconnect", () => {
			// A bad peer most likely
			this.workerManager.removePeer(peer);
			delete this.peers[addr];
			delete this.workers[addr];
		});
	}


	handleRequest(socket) {
		// Create transport
		const transport = new Transport();
		transport.bind(socket);
		// Create peer
		const peer = new PeerClient(this);
		peer.bind(transport);
		this.workerManager.addPeer(peer);
		peer.once("ready", () => {
			// Yay, we know the full address now (after the counterhandshake)
			this.peers[peer.transport.addr] = peer;
			Config.logger.info(`PeerServer: Incoming connection from ${peer.transport.addr}`);
		});
		peer.once("disconnect", () => {
			if(!peer.isReady) {
				// So. We aren't ready, so we don't know the port. So we won't
				// be able to connect to this peer anymore. The worker is still
				// alive, however -- looks like a perfect time to stop it.
				this.workerManager.removePeer(peer);
				delete this.peers[peer.transport.addr];
				delete this.workers[peer.transport.addr];
			}
		});
	}
}