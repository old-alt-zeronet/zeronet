import net from "net";
import {EventEmitter} from "events";
import _Mutex from "await-mutex";
import Config from "../../config";
const Mutex = _Mutex.default; // Someone doesn't know how to use ESM modules

export default class Transport extends EventEmitter {
	constructor() {
		super();
		this.isConnected = false;
		// The correct way to call lock() is to use await; however, we don't
		// really care about it as it'll be unlocked not soon
		this.connectMutex = new Mutex();
		this._connectionUnlockPromise = this.connectMutex.lock();
	}

	// Connect by IP
	connect(addr) {
		const [ip, port] = addr.split(":");
		this.addr = addr;
		this.socket = net.createConnection(+port, ip, async () => {
			this.isConnected = true;
			(await this._connectionUnlockPromise)();
		});
		this.socket.setMaxListeners(100);
		this.socket.on("data", data => {
			this.emit("data", data);
		});
		this.socket.on("error", e => {
			Config.logger.warn(`Connection to ${addr} failed: ${e}`);
			this.isConnected = false;
			this.emit("disconnect");
		});
		this.socket.on("close", () => {
			this.isConnected = false;
			this.emit("disconnect");
		});
		this.isIncoming = false;
	}
	// Bind to an existing socket
	bind(socket) {
		const addr = `${socket.address().address}:unknown`;
		this.addr = addr;
		this.socket = socket;
		socket.setMaxListeners(100);
		socket.on("data", data => {
			this.emit("data", data);
		});
		socket.on("error", e => {
			Config.logger.warn(`Connection to ${addr} failed: ${e}`);
			this.isConnected = false;
			this.emit("disconnect");
		});
		socket.on("close", () => {
			this.isConnected = false;
			this.emit("disconnect");
		});
		this.isIncoming = true;
		this.isConnected = true;
		(async () => {
			(await this._connectionUnlockPromise)();
		})();
	}

	setPort(port) {
		// Replace IP:unknown with IP:port
		const ip = this.addr.split(":")[0];
		this.addr = `${ip}:${port}`;
	}


	async wait() {
		if(!this.isConnected) {
			// A small hack
			const unlock = await this.connectMutex.lock();
			unlock();
		}
	}

	async write(buf) {
		await this.wait();
		if(!this.socket.write(buf)) {
			await new Promise(resolve => this.socket.once("drain", resolve));
		}
	}

	close() {
		this.socket.end();
		this.isConnected = false;
	}
}