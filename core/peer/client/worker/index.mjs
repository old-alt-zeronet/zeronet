import crypto from "crypto";


export default class Worker {
	constructor(peer, manager) {
		this.peer = peer; // Client
		this.blobs = []; // A list of blob IDs that this peer has
		this.sites = []; // A list of sites that this peer has
		this.manager = manager; // Worker manager
		this.currentTask = null; // For debugging
		this.failedTasks = 0;
	}

	init() {
		this.peer.on("ready", this.work.bind(this));
		if(this.peer.isReady) {
			this.work();
		}
	}

	updatePeerData({blobs, sites}) {
		this.blobs = Array.from(new Set(this.blobs.concat(blobs)));
		this.sites = Array.from(new Set(this.sites.concat(sites)));
	}

	async work() {
		while(this.peer.isReady) {
			if(this.peer.bad > this.peer.maxBad) {
				this.peer.disconnect();
				return;
			}

			// Take task
			const task = this.manager.tasks.removeOne(this.canTakeTask.bind(this));
			if(!task) {
				// We can't do anything unfortunately. Wait a few seconds
				await new Promise(resolve => setTimeout(resolve, 1000));
				continue;
			}

			this.currentTask = task;

			await new Promise(async resolve => {
				// Do it
				let result;
				try {
					result = await this.doTask(task, () => {
						// The task no longer depends on peer connection, so we
						// can safely switch to the next task now
						this.currentTask = null;
						resolve();
					});
					// Good!
					this.peer.bad--;
				} catch(e) {
					// We've failed the task :(
					// Sleep a little bit, let other tasks be taken by other peers
					await new Promise(resolve => setTimeout(resolve, 100));
					// We decrease priority, so that bad files don't spoil the queue
					this.failedTasks++;
					this.peer.bad += 5;
					if(task.priority === 0) {
						// This task has failed (most likely)
						console.log(e); // TODO
						return;
					}
					// Return the task to queue
					this.manager.tasks.add({
						...task,
						priority: task.priority - 1
					});
					return;
				}

				// Call callback
				if(task.callback !== null) {
					task.callback(result);
				}
			});
		}
	}


	// Check whether we can take this task
	canTakeTask(task) {
		if(task.type === "getBlob") {
			return (
				this.blobs.indexOf(task.blob) > -1 ||
				this.sites.indexOf(task.site) > -1
			);
		} else if(task.type === "getContent") {
			return this.sites.indexOf(task.site) > -1;
		} else if(task.type === "downloadBlind") {
			return this.sites.indexOf(task.site) > -1;
		} else {
			return false;
		}
	}

	// Do the task
	async doTask(task, release) {
		if(task.type === "getBlob") {
			let content = await this.peer.getBlob(task.blob);
			if(content === null && task.site !== null) {
				// Couldn't download as blob, try as site file
				content = await this.peer.getFile(task.site.address, task.path);
				release(); // We don't depend on peer connection anymore
				if(content !== null) {
					// Check that the ID matches
					const hash = crypto
						.createHash("sha512")
						.update(content)
						.digest()
						.slice(0, 32)
						.toString("hex");
					const id = `${hash},${content.length}`;
					if(id !== task.blob) {
						throw new Error(`Blob ID doesn't match: expected ${task.blob}, got ${id}`);
					}
				}
			}

			if(content === null) {
				throw new Error("Couldn't download blob");
			} else {
				return content;
			}
		} else if(task.type === "getContent") {
			const content = await this.peer.getFile(task.site.address, task.path);
			release(); // We don't depend on peer connection anymore
			if(content === null) {
				// Oopsie
				throw new Error(`Couldn't download ${task.path}`);
			}

			// Ensure that this file is newer than what we had before, and that
			// it's not archived
			let oldContent;
			try {
				oldContent = JSON.parse(await task.site.readFile(task.path));
			} catch(e) {
				oldContent = {modified: -1};
			}
			let archivedBefore;
			try {
				archivedBefore = (await task.site.getFileRules(task.path)).archivedBefore;
			} catch(e) {
				archivedBefore = 0;
			}
			if(oldContent.modified >= content.modified) {
				throw new Error(`${task.path} is too old`);
			}
			if(content.modified <= archivedBefore) {
				throw new Error(`${task.path} is archived`);
			}

			if(await task.site.verifyContent(content, task.path)) {
				// Yay!
				return content;
			} else {
				// Oopsie
				throw new Error(`Couldn't download ${task.path}`);
			}
		} else if(task.type === "downloadBlind") {
			const modifications = await this.peer.listModified(task.site.address);
			release(); // We don't depend on peer connection anymore
			for(const contentPath of Object.keys(modifications)) {
				(async () => {
					const dateModified = modifications[contentPath];
					// Check whether the modification is required (i.e., whether
					// our load version is older). Also check whether the
					// modification is allowed (i.e., the file isn't archived).
					let content;
					try {
						content = await task.site.readContent(contentPath);
					} catch(e) {
						content = {modified: -1};
					}
					let archivedBefore;
					try {
						archivedBefore = (await task.site.getFileRules(contentPath)).archivedBefore;
					} catch(e) {
						archivedBefore = 0;
					}
					if(content.modified < dateModified && dateModified > archivedBefore) {
						// Enqueue
						task.site.downloadTree(contentPath);
					}
				})();
			}
			return null;
		}

		throw new Error(`Unknown task: ${task.type}`);
	}
}