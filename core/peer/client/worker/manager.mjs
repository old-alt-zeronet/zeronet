import PriorityQueue from "fastpriorityqueue";
import Worker from ".";


export default class WorkerManager {
	constructor(discovery) {
		this.tasks = new PriorityQueue((a, b) => a.priority > b.priority);

		this.discovery = discovery;

		this.workers = [];
		// The below is to collapse several equal tasks into a single one. This
		// is required to avoid situations when a file is being downloaded from
		// several peers, and similar ones.
		this.callbacks = {};
	}

	addPeer(peer) {
		const worker = new Worker(peer, this);
		worker.init();
		this.workers.push(worker);
		return worker;
	}
	removePeer(peer) {
		this.workers = this.workers.filter(w => w.peer !== peer);
	}


	enqueue(task, priority=0) {
		const hash = this.hashTask(task, priority);
		if(!this.callbacks[hash]) {
			this.tasks.add({
				priority,
				callback: data => {
					for(const callback of this.callbacks[hash]) {
						callback(data);
					}
					delete this.callbacks[hash];
				},
				...task
			});
		}
	}

	async run(task, priority=0) {
		const hash = this.hashTask(task, priority);
		if(!this.callbacks[hash]) {
			this.callbacks[hash] = [];
			this.tasks.add({
				priority,
				callback: data => {
					for(const callback of this.callbacks[hash]) {
						callback(data);
					}
					delete this.callbacks[hash];
				},
				...task
			});
		}
		return await new Promise(resolve => {
			this.callbacks[hash].push(resolve);
		});
	}

	hashTask(task, priority=0) {
		// Do *not* replace with task.priority = ...! We don't want to change
		// the original task.
		task = {priority, ...task};
		if(task.site) {
			task.site = task.site.address;
		}
		return JSON.stringify(task);
	}


	async ensurePeersForSite(site, peerServer) {
		if(this.workers.filter(worker => worker.sites.indexOf(site) > -1) < 5) {
			console.log("Announcing");
			await this.discovery.announceSite(site, peerServer, true);
		}
	}
}