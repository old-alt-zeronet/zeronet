import msgpack from "msgpack-lite";
import {EventEmitter} from "events";
import Transport from "../transport";
import Config from "../../../config";

export default class PeerClient extends EventEmitter {
	constructor(peerServer) {
		super();
		this.setMaxListeners(1000);
		this.peerServer = peerServer;
		this.bad = 0; // Peer rating (negative)
		this.maxBad = 100; // Max possible rating
		this.isReady = false;
		this.reqId = 0; // Last request id
		this.waitingResponses = {};
	}

	// Add & connect by IP
	connect(addr) {
		this.transport = new Transport();
		this.transport.connect(addr);
		this.transport.on("disconnect", () => {
			this.emit("disconnect");
		});
		this.startLoop();
		this.handshake();
	}
	// Connect to an existing socket
	bind(transport) {
		this.transport = transport;
		this.transport.on("disconnect", () => {
			this.emit("disconnect");
		});
		this.startLoop();
	}

	startLoop() {
		// Init message loop
		const unpacker = msgpack.createDecodeStream();
		this.transport.on("data", data => {
			unpacker.write(data);
		});
		unpacker.on("data", data => {
			this.handleMessage(data);
		});
	}



	// Utility functions
	toString() {
		return `[PeerClient ${this.transport.addr}]`;
	}
	// Raw socket send/receive
	async send(message) {
		await this.transport.write(msgpack.encode(message));
	}
	async request(cmd, params={}) {
		const reqId = ++this.reqId;
		return await new Promise(resolve => {
			this.waitingResponses[reqId] = resolve;
			this.send({
				cmd,
				req_id: reqId, // eslint-disable-line camelcase
				params
			});
		});
	}
	async response(to, message) {
		if(typeof message !== "object" || Array.isArray(message) || message === null) {
			message = {body: message};
		}
		message.cmd = "response";
		message.to = to;
		await this.send(message);
	}
	disconnect() {
		this.transport.close();
		this.bad = Infinity;
		this.isReady = false;
		this.emit("disconnect");
	}


	handshake() {
		/* eslint-disable camelcase */
		this.send({
			cmd: "handshake",
			req_id: 0,
			params: this.generateHandshake()
		});
		/* eslint-enable camelcase */
	}
	generateHandshake() {
		/* eslint-disable camelcase */
		return {
			version: Config.version,
			protocol: "v2", // ???
			use_bin_type: true, // We don't really care about it
			peer_id: this.peerServer.peerId,
			fileserver_port: this.peerServer.port,
			port_opened: null, // TODO: set true/false
			target_ip: this.transport.addr.split(":")[0],
			rev: Config.rev,
			crypt_supported: [], // TODO: support crypt
			crypt: null, // TODO: support crypt
			time: Math.floor(Date.now() / 1000)
		};
		/* eslint-enable camelcase */
	}

	handleMessage(message) {
		if(
			typeof message !== "object" ||
			message === null ||
			typeof message.cmd !== "string"
		) {
			Config.logger.warn(`${this}: Invalid message`);
			this.bad += 10;
			return;
		}

		if(message.cmd === "response" && message.to === 0) {
			// Counterhandshake
			if(message.protocol !== "v2") {
				// We don't support this protocol
				Config.logger.warn(`${this}: Unsupported protocol ${message.protocol}`);
				this.disconnect();
				return;
			}
			this.transport.setPort(message.fileserver_port);
			this.isReady = true;
			this.emit("ready");
		} else if(message.cmd === "response") {
			// Response to a simple command
			if(this.waitingResponses[message.to]) {
				const cb = this.waitingResponses[message.to];
				delete this.waitingResponses[message.to];
				cb(message);
			} else {
				// We didn't ask for this!
				Config.logger.warn(`${this}: Response to ghost message`);
				this.bad += 2;
			}
		} else {
			const cmd = message.cmd;
			const key = `command${cmd[0].toUpperCase()}${cmd.substr(1)}`;
			if(!this[key]) {
				this.response(
					message.req_id,
					{
						error: `Unknown command: ${cmd}`
					}
				);
				this.bad++;
				return;
			}
			this[key](message.req_id, message.params);
		}
	}


	// Remote commands
	commandHandshake(id, handshake) {
		if(handshake.protocol !== "v2") {
			// We don't support this protocol
			Config.logger.warn(`${this}: Unsupported protocol ${handshake.protocol}`);
			this.disconnect();
			return;
		}
		this.transport.setPort(handshake.fileserver_port);
		this.isReady = true;
		this.emit("ready");
		// Reply with handshake
		this.response(id, this.generateHandshake());
	}


	// Commands
	async getFile(
		siteAddress,
		filePath,
		{offset, offsetEnd, forceStreaming}={
			offset: 0,
			offsetEnd: null,
			forceStreaming: false
		}
	) {
		// TODO: Implement forceStreaming
		forceStreaming; // eslint-disable-line no-unused-expressions

		const blockSize = 1024 * 1024;

		const parts = [];
		while(offsetEnd === null ? true : offset < offsetEnd) {
			/* eslint-disable camelcase */
			const block = await this.request("getFile", {
				site: siteAddress,
				inner_path: filePath,
				location: offset,
				read_bytes: (
					offsetEnd === null
						? blockSize
						: Math.min(blockSize, offsetEnd - offset)
				),
				file_size: null
			});
			/* eslint-enable camelcase */
			if(block.error) {
				// Oopsie
				return null;
			}
			parts.push(block.body);
			if(block.location === block.size || block.location === offsetEnd) {
				// Done
				break;
			}
			if(block.location === offset) {
				// The pointer hasn't even moved!
				Config.logger.warn(`${this}: File pointer didn't move at ${filePath}`);
				this.bad += 5;
				return null;
			}
			offset = block.location;
		}
		return Buffer.concat(parts);
	}

	// eslint-disable-next-line require-await, no-unused-vars
	async getBlob(id) {
		// TODO: Not supported yet
		return null;
	}

	async listModified(siteAddress, since=null) {
		const result = await this.request("listModified", {
			since,
			site: siteAddress
		});
		if(result.error) {
			return {};
		}
		return result.modified_files;
	}
}