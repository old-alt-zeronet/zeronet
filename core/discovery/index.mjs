import {EventEmitter} from "events";
import Config from "../../config";


export default class Discovery extends EventEmitter {
	async announceSite(site, peerServer, fast=false) {
		await this._announce(
			site.address,
			site.toString(),
			{
				blobs: [],
				sites: [site]
			},
			peerServer,
			fast
		);
	}
	async announceBlob(id, peerServer, fast=false) {
		await this._announce(
			`blob:${id}`,
			`[Blob ${id}]`,
			{
				blobs: [id],
				sites: []
			},
			peerServer,
			fast
		);
	}


	async _announce(id, name, peerData, peerServer, fast=false) {
		Config.logger.info(`Announcing ${name} (${fast ? "fast" : "slow"})`);

		let remaining = Config.trackers.length;
		let cntGood = 0;

		await Promise.race(
			Config.trackers.map(async tracker => {
				try {
					const addrs = (await this.announceSingleTracker(id, tracker, peerServer, fast))
						.filter(addr => !addr.endsWith(":0") && !addr.endsWith(":1"));
					if(addrs.length > 0) {
						// Connect to the peers
						for(const addr of addrs) {
							this.emit("newPeer", addr, peerData);
						}
						cntGood++;
						return;
					}
				} catch(e) {
					Config.logger.warn(`Failed to announce ${name} to ${tracker}: ${e}`);
				}
				remaining--;
				if(remaining > 0) {
					// Probably another tracker will succeed??
					await new Promise(() => {});
				}
			})
		);

		if(cntGood > 0) {
			Config.logger.info(`Announced ${name} to ${cntGood} tracker(s)`);
		} else {
			Config.logger.warn(`Failed to announce ${name}`);
		}
	}


	// The function below is a stub -- it should be implemented by plugins
	// eslint-disable-next-line no-unused-vars, require-await
	async announceSingleTracker(id, tracker, fast) {
		Config.logger.warn(`Unknown tracker protocol: ${tracker}`);
		return [];
	}
}