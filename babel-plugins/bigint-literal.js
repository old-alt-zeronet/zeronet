const syntaxBigInt = require("@babel/plugin-syntax-bigint").default;

module.exports = babel => {
	return {
		inherits: syntaxBigInt,
		visitor: {
			BigIntLiteral(path) {
				const number = Number(path.node.value);
				let arg;
				if(Number.MIN_SAFE_INTEGER <= number && number <= Number.MAX_SAFE_INTEGER) {
					// 1n -> BigInt(1)
					arg = babel.types.numericLiteral(number);
				} else {
					// 9007199254740993n -> BigInt("9007199254740993")
					arg = babel.types.stringLiteral(path.node.value);
				}
				path.replaceWith(
					babel.types.callExpression(
						babel.types.identifier("BigInt"),
						[arg]
					)
				);
			}
		}
	};
};