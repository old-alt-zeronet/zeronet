module.exports = {
	plugins: [
		"@babel/plugin-transform-modules-commonjs",
		"@babel/plugin-syntax-bigint",
		"./babel-plugins/bigint-literal"
	]
};